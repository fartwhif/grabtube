﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace GrabTube
{
    public class Playlist : INotifyPropertyChanged
    {
        public override int GetHashCode()
        {
            return Id;
        }
        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(Playlist))
            {
                return false;
            }

            return Id == ((Playlist)obj).Id;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public int _NumVideos = 0;
        public int NumVideos { get => _NumVideos; set { _NumVideos = value; OnNotifyPropertyChanged(); } }
        public List<string> MetaInfo { get; set; }
        public List<Vid> Videos { get; set; }
        public string UrlThumb { get; set; }
        private string _Status = string.Empty;
        public string Status { get => _Status; set { _Status = value; OnNotifyPropertyChanged(); } }
        public PlaylistType Type { get; set; }
        public bool Grab { get; set; }
        public Playlist(int Id, string Name, string Url, int NumVideos, List<string> MetaInfo, string UrlThumb, bool grab)
        {
            Grab = grab;
            this.Id = Id;
            this.Name = Name;
            this.Url = Url;
            this.NumVideos = NumVideos;
            this.MetaInfo = MetaInfo;
            this.UrlThumb = UrlThumb;
        }
        public Playlist()
        {
            Id = -1;
            Name = string.Empty;
            Url = string.Empty;
            NumVideos = 0;
            MetaInfo = new List<string>();
            UrlThumb = string.Empty;
            Status = string.Empty;
            Type = PlaylistType.Normal;
        }
        public override string ToString()
        {
            return string.Format("Playlist \"{0}\", Entries: {1}", Name, NumVideos, Status);
        }

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        protected void OnNotifyPropertyChanged([CallerMemberName] string memberName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(memberName));
        }
    }
}
