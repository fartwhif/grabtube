﻿using GrabTube.ViewModels;
using System;
using System.Diagnostics;

namespace GrabTube.Model
{
    internal class Player : IDisposable
    {
        public Process PlayerProcess = null;
        public void Play(Vid vid, Options options)
        {
            Stop();
            PlayerProcess = MainWindowPresenter.PlayMedia(vid.FilePath, options);
            CurrentlyPlaying = vid;
            PlayTimer.Restart();
        }
        public bool Playing
        {
            get
            {
                if (PlayerProcess != null)
                {
                    return !PlayerProcess.HasExited;
                }

                Process[] pname = Process.GetProcessesByName("mpc-hc64");
                Process[] pname2 = Process.GetProcessesByName("mpc-hc64_nvo");
                Process[] pname3 = Process.GetProcessesByName("mpc-hc");
                Process[] pname4 = Process.GetProcessesByName("mpc-hc_nvo");
                if (pname.Length == 0 && pname2.Length == 0 && pname3.Length == 0 && pname4.Length == 0)
                {
                    return false;
                }

                return true;
            }
        }
        public void Stop()
        {
            PlayTimer.Stop();
            if (PlayerProcess != null)
            {
                if (Playing)
                {
                    try { PlayerProcess.Kill(); }
                    catch (Exception) { }
                }
            }
        }
        public TimeSpan Remaining
        {
            get
            {
                if (!Playing)
                {
                    return TimeSpan.Zero;
                }

                return CurrentlyPlaying.LengthTS - PlayTimer.Elapsed;
            }
        }
        public Stopwatch PlayTimer = new Stopwatch();
        public Vid CurrentlyPlaying = null;
        public void Dispose()
        {
            //Stop();
        }
    }
}
