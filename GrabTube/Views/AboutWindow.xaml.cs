﻿using GrabTube.ViewModels;
using System.Windows;

namespace GrabTube.Views
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : Window
    {
        public AboutWindowPresenter ViewModel = null;
        public AboutWindow()
        {
            InitializeComponent();
            ViewModel = new AboutWindowPresenter(this);
            DataContext = ViewModel;
        }
        public void Show(MainWindow win)
        {
            ViewModel.Show(win);
        }
    }

}
