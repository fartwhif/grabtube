﻿using GrabTube.ViewModels;
using System.Windows;

namespace GrabTube
{
    /// <summary>
    /// Interaction logic for StringInputWindow.xaml
    /// </summary>
    public partial class StringInputWindow : Window
    {
        public StringInputWindowPresenter ViewModel = null;
        public StringInputWindow(StringInputWindowPresenter.StringInputWindowData Data)
        {
            InitializeComponent();
            ViewModel = new StringInputWindowPresenter(this, Data);
            DataContext = ViewModel;
        }
        public bool? ShowDialog(Window win)
        {
            return ViewModel.ShowDialog(win);
        }
    }
}
