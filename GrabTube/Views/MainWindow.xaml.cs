﻿using GrabTube.ViewModels;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;


namespace GrabTube.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindowPresenter _ViewModel = null;
        public MainWindowPresenter ViewModel => _ViewModel;
        public MainWindow()
        {
            InitializeComponent();
            _ViewModel = new MainWindowPresenter(this);
            DataContext = ViewModel;
        }

        private void lvVideos_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            System.Collections.IList t = e.AddedItems;
            if (t.Count > 0)
            {
                ViewModel.SetSelectedVideo((Vid)t[0]);
            }
            else
            {
                ViewModel.SetSelectedVideo(null);
            }
        }

        private void lvLists_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            System.Collections.IList t = e.AddedItems;
            if (t.Count > 0)
            {
                ViewModel.SetSelectedPlaylist((Playlist)t[0]);
            }
            else
            {
                ViewModel.SetSelectedPlaylist(null);
            }
        }

        private void btnShare_Click(object sender, RoutedEventArgs e)
        {
            Vid vid = (Vid)((FrameworkElement)sender).Tag;
            ViewModel.ShareVideo(vid);
        }


        private void btnWatch_Click(object sender, RoutedEventArgs e)
        {
            Vid vid = (Vid)((FrameworkElement)sender).Tag;
            ViewModel.Watch(vid);
        }

        private void btnWeb_Click(object sender, RoutedEventArgs e)
        {
            Vid vid = (Vid)((FrameworkElement)sender).Tag;
            ViewModel.GoToPage(vid);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            Playlist pl = (Playlist)((FrameworkElement)sender).Tag;
            ViewModel.UpdateList(pl);
        }

        private void GridViewColumnHeader_Videos(object sender, RoutedEventArgs e)
        {
            if (sender == null)
            {
                return;
            }

            GridViewColumnHeader col = (GridViewColumnHeader)sender;
            ViewModel.VideosColumnClicked(col);
        }

        private void GridViewColumnHeader_Lists(object sender, RoutedEventArgs e)
        {
            if (sender == null)
            {
                return;
            }

            GridViewColumnHeader col = (GridViewColumnHeader)sender;
            ViewModel.ListsColumnClicked(col);
        }

        private void btnWeb_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                Vid vid = (Vid)((FrameworkElement)sender).Tag;
                if (!File.Exists(vid.FilePath))
                {
                    return;
                }

                Process.Start("explorer.exe", "/select, \"" + vid.FilePath + "\"");
            }
            catch (Exception) { }
        }




    }
}
