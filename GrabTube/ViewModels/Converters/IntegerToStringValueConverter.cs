﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace GrabTube.ViewModels.Converters
{
    public class IntegerToStringValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || value.GetType() != typeof(int))
            {
                return null;
            }

            return ((int)value).ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try { return int.Parse((string)value); }
            catch { return 0; }
        }
    }
}
