﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace GrabTube.ViewModels.Converters
{
    public class FormattedMultiStringsValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter == null || parameter.GetType() != typeof(string))
            {
                return null;
            }

            if (string.IsNullOrWhiteSpace((string)values[0]) && string.IsNullOrWhiteSpace((string)values[1]))
            {
                return null;
            }

            return string.Format(parameter.ToString(), values);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            //throw new NotImplementedException();
            return null;
        }
    }
}
