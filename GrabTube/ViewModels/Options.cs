﻿using SafeConfig;
using System.Net;

namespace GrabTube.ViewModels
{
    public class Options
    {
        public int MonitorNumber { get; set; }
        public bool D3dfs { get; set; }
        private bool _ShowBrowser = false;
        public bool ShowBrowser { get => _ShowBrowser; set { if (_ShowBrowser != value) { _ShowBrowser = value; Global.BrowserReady.WaitOne(); Global.BrowserSetShowBrowser(value); } } }
        public bool DefaultVidDev { get; set; }
        public int FeedWatchServerPort { get; set; }
        public bool FetchMissingVideoFiles { get; set; }
        public string FeedWatchServerListenIP { get; set; }
        public Options()
        {
            FeedWatchServerListenIP = "0.0.0.0";
            FeedWatchServerPort = 21356;
            FetchMissingVideoFiles = true;
            MonitorNumber = 1;
            D3dfs = false;
            ShowBrowser = false;
            DefaultVidDev = true;
        }
        public void Save(ConfigManager cfg)
        {
            try
            {
                cfg.Set("FeedWatchServerListenIP", FeedWatchServerListenIP);
                cfg.Set("FeedWatchServerPort", FeedWatchServerPort);
                cfg.Set("FetchMissingVideoFiles", FetchMissingVideoFiles);
                cfg.Set("MonitorNumber", MonitorNumber);
                cfg.Set("D3dfs", D3dfs);
                cfg.Set("ShowBrowser", ShowBrowser);
                cfg.Set("DefaultVidDev", DefaultVidDev);
            }
            catch
            {

            }
        }
        public void Load(ConfigManager cfg)
        {
            try
            {
                FeedWatchServerListenIP = cfg.Get<string>("FeedWatchServerListenIP");
                if (string.IsNullOrEmpty(FeedWatchServerListenIP))
                {
                    FeedWatchServerListenIP = "0.0.0.0";
                }

                if (!IPAddress.TryParse(FeedWatchServerListenIP, out IPAddress r))
                {
                    FeedWatchServerListenIP = "0.0.0.0";
                }

                FeedWatchServerPort = cfg.Get<int>("FeedWatchServerPort");
                if (FeedWatchServerPort == 0)
                {
                    FeedWatchServerPort = 80;
                }

                if (FeedWatchServerPort > 65534)
                {
                    FeedWatchServerPort = 80;
                }

                MonitorNumber = cfg.Get<int>("MonitorNumber");
                D3dfs = cfg.Get<bool>("D3dfs");
                DefaultVidDev = cfg.Get<bool>("DefaultVidDev");
                ShowBrowser = cfg.Get<bool>("ShowBrowser");
                FetchMissingVideoFiles = cfg.Get<bool>("FetchMissingVideoFiles");

            }
            catch
            {

            }
        }
    }
}
