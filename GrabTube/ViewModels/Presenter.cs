﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Threading;

namespace GrabTube.ViewModels
{
    public class Presenter : INotifyPropertyChanged
    {
        private readonly Dispatcher disp = null;
        public Presenter(FrameworkElement fe)
        {
            disp = fe.Dispatcher;
        }
        public bool InvokeIfNecessaryWithRes(Delegate act, ref object result, params object[] args)
        {
            if (disp.CheckAccess())
            {
                return false;
            }
            result = disp.Invoke(act, args);
            return true;
        }
        public bool InvokeIfNecessary(Delegate act, params object[] args)
        {
            if (disp.CheckAccess())
            {
                return false;
            }
            disp.Invoke(act, args);
            return true;
        }
        public bool InvokeAsyncIfNecessary(Delegate act, params object[] args)
        {
            if (disp.CheckAccess())
            {
                return false;
            }
            disp.BeginInvoke(act, args);
            return true;
        }
        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        public void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
