﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YoutubeExplode;
using YoutubeExplode.Channels;
using YoutubeExplode.Videos;
using YoutubeExplode.Videos.ClosedCaptions;
using YoutubeExplode.Videos.Streams;

namespace GrabTube.ViewModels
{
    /// <summary>
    /// adapted from YoutubeExplode examples (LGPL)
    /// </summary>
    public class ExplodingDownloader
    {
        public readonly YoutubeClient Client;
        public bool IsBusy { get; set; } = false;
        public Video Video { get; set; } = null;
        public Channel Channel { get; set; } = null;
        public string Query { get; set; } = null;
        public IReadOnlyList<MuxedStreamInfo> MuxedStreamInfos { get; set; } = null;
        public IReadOnlyList<AudioOnlyStreamInfo> AudioOnlyStreamInfos { get; set; } = null;
        public IReadOnlyList<VideoOnlyStreamInfo> VideoOnlyStreamInfos { get; set; } = null;
        public IReadOnlyList<ClosedCaptionTrackInfo> ClosedCaptionTrackInfos { get; set; } = null;

        public ExplodingDownloader(YoutubeClient client)
        {
            Client = client;
        }
        public async Task PullData()
        {
            try
            {
                // Enter busy state
                IsBusy = true;

                // Reset data
                Video = null;
                Channel = null;
                MuxedStreamInfos = null;
                AudioOnlyStreamInfos = null;
                VideoOnlyStreamInfos = null;
                ClosedCaptionTrackInfos = null;

                // Normalize video id
                YoutubeExplode.Videos.VideoId videoId = new YoutubeExplode.Videos.VideoId(Query!);

                // Get data
                StreamManifest streamManifest = await Client.Videos.Streams.GetManifestAsync(videoId);
                ClosedCaptionManifest trackManifest = await Client.Videos.ClosedCaptions.GetManifestAsync(videoId);

                Video = await Client.Videos.GetAsync(videoId);
                Channel = await Client.Channels.GetByVideoAsync(videoId);
                MuxedStreamInfos = streamManifest.GetMuxed().ToArray();
                AudioOnlyStreamInfos = streamManifest.GetAudioOnly().ToArray();
                VideoOnlyStreamInfos = streamManifest.GetVideoOnly().ToArray();
                ClosedCaptionTrackInfos = trackManifest.Tracks;
            }
            finally
            {
                // Exit busy state
                IsBusy = false;
            }
        }

        public async void DownloadStream(string filePath, IStreamInfo streamInfo, Progress<double> progressHandler)
        {
            try
            {
                // Enter busy state
                IsBusy = true;

                if (string.IsNullOrWhiteSpace(filePath))
                {
                    return;
                }

                // Download to file
                await Client.Videos.Streams.DownloadAsync(streamInfo, filePath, progressHandler);
            }
            finally
            {
                // Exit busy state
                IsBusy = false;
            }
        }
        public async void DownloadClosedCaptionTrack(string filePath, ClosedCaptionTrackInfo trackInfo, Progress<double> progressHandler)
        {
            try
            {
                // Enter busy state
                IsBusy = true;
                // Download to file
                await Client.Videos.ClosedCaptions.DownloadAsync(trackInfo, filePath, progressHandler);
            }
            finally
            {
                // Exit busy state
                IsBusy = false;
            }
        }
    }
}
