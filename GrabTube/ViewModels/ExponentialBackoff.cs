﻿using System;
using System.Threading.Tasks;

namespace GrabTube.ViewModels
{
    public struct ExponentialBackoff
    {
        private readonly int _delayMilliseconds;
        private readonly int _maxDelayMilliseconds;
        private int _retries;
        private int _pow;
        private const int RetryCountMax = 31;

        public ExponentialBackoff(int delayMilliseconds, int maxDelayMilliseconds)
        {
            _delayMilliseconds = delayMilliseconds;
            _maxDelayMilliseconds = maxDelayMilliseconds;
            _retries = 0;
            _pow = 1;
        }
        public void Reset()
        {
            _retries = 0;
            _pow = 1;
        }
        public async Task Delay()
        {
            ++_retries;
            if (_retries < RetryCountMax)
            {
                _pow <<= 1;
            }
            var delay = Math.Min(_delayMilliseconds * (_pow - 1) / 2, _maxDelayMilliseconds);
            await Task.Delay(delay);
        }
    }
}
