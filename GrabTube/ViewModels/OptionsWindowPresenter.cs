﻿using GrabTube.Views;

namespace GrabTube.ViewModels
{
    public class OptionsWindowPresenter : Presenter
    {
        private readonly OptionsWindow win = null;
        public OptionsWindowPresenter(OptionsWindow win)
          : base(win)
        {
            this.win = win;
        }

        public string Version => Constants.version;
        public string Product => Constants.appName;
        private delegate void voidMainWindowDelegate(MainWindow mw);
        public MainWindow MainWin { get; set; }
        public void Show(MainWindow win)
        {
            if (InvokeIfNecessary(new voidMainWindowDelegate(Show), win))
            {
                return;
            }

            MainWin = win;
            this.win.Owner = win;
            this.win.ShowDialog();
        }
    }
}
