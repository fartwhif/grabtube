﻿using System.Windows;
using System.Windows.Input;

namespace GrabTube.ViewModels
{
    public class StringInputWindowPresenter : Presenter
    {
        public class StringInputWindowData
        {
            public string Title { get; set; }
            public string ButtonText { get; set; }
            public string Input { get; set; }
            public bool Result { get; set; }
        }
        public StringInputWindowData Data { get; set; }

        private readonly StringInputWindow win = null;
        public StringInputWindowPresenter(StringInputWindow win, StringInputWindowData Data)
            : base(win)
        {
            this.win = win;
            this.Data = Data;
        }

        private bool CanOk => true;

        private RelayCommand _OkCommand; public ICommand OkCommand { get { if (_OkCommand == null) { _OkCommand = new RelayCommand(param => Ok(), param => CanOk); } return _OkCommand; } }
        private void Ok()
        {
            win.DialogResult = true;
        }
        private delegate bool? boolishWindowDele(Window win);
        public bool? ShowDialog(Window owner)
        {
            object res = false;
            if (InvokeIfNecessaryWithRes(new boolishWindowDele(ShowDialog), ref res, owner))
            {
                return (bool?)res;
            }

            win.Owner = owner;
            return win.ShowDialog();
        }
    }
}
