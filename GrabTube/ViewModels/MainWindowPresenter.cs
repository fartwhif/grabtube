﻿using GrabTube.Model;
using GrabTube.Views;
using HtmlAgilityPack;
using HTMLConverter;
using SafeConfig;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using WatiN.Core;
using YoutubeExplode.Exceptions;
using YoutubeExplode.Videos;
using YoutubeExplode.Videos.Streams;

namespace GrabTube.ViewModels
{
    public partial class MainWindowPresenter : Presenter
    {
        private SQLiteConnection db = null;
        private ConfigManager cfg = null;
        private readonly MainWindow win = null;
        private readonly ICookieJarFiller cookieFiller = null;
        public MainWindowPresenter(MainWindow win)
            : base(win)
        {
            this.win = win;
            if (!Init_cfg())
            {
                Exit();
                return;
            }
            this.win.Loaded += Win_Loaded;
            this.win.Closing += Win_Closing;
            Lists = new ObservableCollection<Playlist>();
            Global.Instance.MainWindowDispatcher = Dispatcher.CurrentDispatcher;
            Global.Instance.MainWin = win;
            Global.Instance.ShowBrowser = Settings.ShowBrowser;
            Global.StartBrowser();
            Settings.Load(cfg);
            cookieFiller = new WininetWrapper();
        }
        private bool Init_cfg()
        {
            try
            {
                cfg = new ConfigManager()
                    .WithCurrentUserScope()
                    .WithEntropy(Entropy)
                    .AtFolder(Path_data)
                    .Load();
                return true;
            }
            catch (Exception)
            {
                try
                {
                    var b = Path.Combine(Path_data, "settings.saveconfig");
                    if (File.Exists(b))
                    {
                        File.Delete(b);
                        cfg = new ConfigManager()
                            .WithCurrentUserScope()
                            .WithEntropy(Entropy)
                            .AtFolder(Path_data)
                            .Load();
                        return true;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                return false;
            }
        }
        private void Win_Closing(object sender, CancelEventArgs e)
        {
            if (db != null)
            {
                try { db.Close(); }
                catch { }
            }
            Disposables.ToList().ForEach(k => { if (k != null) { k.Dispose(); } });
            closed = true;
            cancelled = true;
            FeedWatchCanceller.Set();
            Settings.Save(cfg);
            cfg.Save();
            BrowserShutdown();
        }
        private bool closed = false;
        private readonly CookieContainer CookieJar = null;
        private void Win_Loaded(object sender, RoutedEventArgs e)
        {
            bool newDb = false;
            if (!GrabDbExists)
            {
                StartNewDb();
                newDb = true;
            }
            OpenDb();
            if (newDb)
            {
                Login();
                UpdateLibraryAndLists();
            }

            if (CookieJar == null)
            {
                FillCookieJar();
            }
            win.lvLists.ItemsSource = Lists;
            ListsView = CollectionViewSource.GetDefaultView(Lists);
            if (!GrabFolderExists)
            {
                SetGrabFolder(true);
            }

            Global.Instance.GrabFolder = GrabFolder;
            SyncLists(GetLists(db), null, null, null);
            updateTimer = new Timer((r) =>
            {
                Tick();
            });
            updateTimer.Change(0, 1000);
            win.fdrDescription.MouseWheel += FdrDescription_MouseWheel;
            win.fdrDescription.PreviewMouseWheel += FdrDescription_PreviewMouseWheel;
            BrowserShell.Instance.OnDownloadException += Instance_OnDownloadException;
            BrowserShell.Instance.OnDownloadAttemptInner += Instance_OnDownloadAttemptInner;
            BrowserShell.Instance.OnDownloadAttemptOuter += Instance_OnDownloadAttemptOuter;
        }
        private void FillCookieJar()
        {
            new WininetWrapper(Constants.urlAcct).FillCookieJar();
        }
        private void Instance_OnDownloadAttemptOuter(string url)
        {
            outer = url;
        }
        private void Instance_OnDownloadAttemptInner(HttpWebRequest req)
        {
            inner = req;
        }
        private string outer;
        private HttpWebRequest inner;
        private void Instance_OnDownloadException(Exception ex)
        {
            string url = string.Empty;
            if (ex.GetType() == typeof(System.Net.WebException))
            {
                WebException a = ex as System.Net.WebException;
                url = $"response url: {a.Response?.ResponseUri?.OriginalString}";
            }
            string b = $"{DateTime.Now.ToString()} ERROR:{Environment.NewLine}VidUrl: {outer}{Environment.NewLine}{ex.Message}{Environment.NewLine}{url}{Environment.NewLine}{Environment.NewLine}";
            File.AppendAllText(Path.Combine(Path_data, "errors.log"), b);
        }
        public void Search(string SearchInput)
        {
            if (string.IsNullOrWhiteSpace(SearchInput)) { SetSelectedPlaylist(null); return; }
            string inp = SearchInput.Trim().ToLower();
            List<Vid> Videos = Lists.SelectMany(k => k.Videos).ToList();
            List<Vid> SearchResults = Videos.Where(k => { return k.Author.ToLower().Contains(inp) || k.Title.ToLower().Contains(inp) || k.Description.ToLower().Contains(inp) || k.Keywords.ToLower().Contains(inp); })
                .GroupBy(k => k.Id)
                .Select(g => g.First())
                .ToList();
            Playlist pl = new Playlist() { Name = "Search Results" };
            pl.Videos = SearchResults;
            pl.NumVideos = pl.Videos.Count;
            SetSelectedPlaylist(pl);
        }
        private GridViewColumnHeader listViewSortCol_Videos = null;
        private SortAdorner listViewSortAdorner_Videos = null;
        private GridViewColumnHeader listViewSortCol_Lists = null;
        private SortAdorner listViewSortAdorner_Lists = null;
        public void VideosColumnClicked(GridViewColumnHeader column)
        {
            string sortBy = column.Tag.ToString();
            if (listViewSortCol_Videos != null)
            {
                AdornerLayer.GetAdornerLayer(listViewSortCol_Videos).Remove(listViewSortAdorner_Videos);
                win.lvVideos.Items.SortDescriptions.Clear();
            }
            ListSortDirection newDir = ListSortDirection.Ascending;
            if (listViewSortCol_Videos == column && listViewSortAdorner_Videos.Direction == newDir)
            {
                newDir = ListSortDirection.Descending;
            }
            listViewSortCol_Videos = column;
            listViewSortAdorner_Videos = new SortAdorner(listViewSortCol_Videos, newDir);
            AdornerLayer.GetAdornerLayer(listViewSortCol_Videos).Add(listViewSortAdorner_Videos);
            win.lvVideos.Items.SortDescriptions.Add(new SortDescription(sortBy, newDir));
        }
        public void ListsColumnClicked(GridViewColumnHeader column)
        {
            string sortBy = column.Tag.ToString();
            if (listViewSortCol_Lists != null)
            {
                AdornerLayer.GetAdornerLayer(listViewSortCol_Lists).Remove(listViewSortAdorner_Lists);
                win.lvLists.Items.SortDescriptions.Clear();
            }
            ListSortDirection newDir = ListSortDirection.Ascending;
            if (listViewSortCol_Lists == column && listViewSortAdorner_Lists.Direction == newDir)
            {
                newDir = ListSortDirection.Descending;
            }
            listViewSortCol_Lists = column;
            listViewSortAdorner_Lists = new SortAdorner(listViewSortCol_Lists, newDir);
            AdornerLayer.GetAdornerLayer(listViewSortCol_Lists).Add(listViewSortAdorner_Lists);
            win.lvLists.Items.SortDescriptions.Add(new SortDescription(sortBy, newDir));
        }
        private ICollectionView ListsView = null;
        private void FdrDescription_PreviewMouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            //throw new NotImplementedException();
        }
        private void FdrDescription_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            //win.svDescription.ScrollToVerticalOffset(e.o)
            //ScrollView.ScrollToVerticalOffset(ScrollView.VerticalOffset - e.GetCurrentPoint(ScrollView).Properties.MouseWheelDelta);
        }
        private void Exit()
        {
            win.Close();
        }
        private void OpenDb()
        {
            if (db == null)
            {
                db = new SQLiteConnection("Data Source='" + GrabDbPath + "';Version=3;");
                db.Open();
            }
        }
        private void StartNewDb()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "GrabTube.Resources.grab.db";
            byte[] dbTemplate = null;
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    stream.CopyTo(ms);
                    dbTemplate = ms.ToArray();
                }
            }
            File.WriteAllBytes(GrabDbPath, dbTemplate);
        }
        private bool GrabFolderExists => Directory.Exists(GrabFolder);
        private bool GrabDbExists => File.Exists(GrabDbPath);
        private string GrabDbPath => Path.Combine(Path_data, "grab.db");
        private string GrabFolder { get => cfg.Get<string>("grabfolder"); set => cfg.Set("grabfolder", value); }
        private void SetGrabFolder(bool ExitIfCancel)
        {
            using System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog
            {
                Description = "Set the grab (download) folder."
            };
            if (GrabFolderExists)
            {
                dialog.SelectedPath = GrabFolder;
            }
            System.Windows.Forms.DialogResult result = dialog.ShowDialog(win.GetIWin32Window());
            if (result != System.Windows.Forms.DialogResult.OK)
            {
                if (ExitIfCancel)
                {
                    MessageBox.Show(win, "You must set a grab (download) folder to use " + Constants.appName);
                    win.Close();
                }
                return;
            }
            GrabFolder = dialog.SelectedPath;
            Global.Instance.GrabFolder = dialog.SelectedPath;
        }
        private void SingleGrabStart()
        {
            if (Busy)
            {
                return;
            }
            string txt = "";
            try { txt = Clipboard.GetText(); } catch { }
            if (!string.IsNullOrWhiteSpace(txt))
            {
                txt = GetVideoIdFromUrl(txt);
                if (txt != null)
                {
                    txt = GetVideoUrlFromId(txt);
                }
            }
            StringInputWindow siw = new StringInputWindow(new StringInputWindowPresenter.StringInputWindowData() { Title = "Single Grab - Enter Youtube video URL", ButtonText = "OK", Input = txt });
            bool? res = siw.ShowDialog(win);
            if (res.HasValue && res.Value)
            {
                string url = siw.ViewModel.Data.Input;
                if (string.IsNullOrWhiteSpace(url))
                {
                    return;
                } (new Thread((r) => { SingleGrab(url); }) { ApartmentState = ApartmentState.STA }).Start();
            }
        }
        private delegate void voidStringDelegate(string str);
        private void Message(string msg)
        {
            if (InvokeIfNecessary(new voidStringDelegate(Message), msg))
            {
                return;
            }
            MessageBox.Show(win, msg);
        }
        private async void SingleGrab(string url)
        {
            Busy = true;
            cancelled = false;
            if (string.IsNullOrWhiteSpace(url))
            {
                return;
            }
            Playlist pl = EnsureList(new Playlist() { Grab = false, Name = "single grabs", Type = (int)PlaylistType.Normal });
            Func<string, double, bool> progress = (t, d) => { return UpdateProgress(null, pl, t, d); };
            Vid v = GetExistingData(GetVideoUrlFromId(url), db);
            if (!string.IsNullOrWhiteSpace(v.Url))
            {
                if (pl.Videos.Any(k => k.Id == v.Id))
                {
                    Message("This video is already in the grabs folder.");
                    Busy = false;
                    return;
                }
                else
                {
                    InsertListVideoLookup(db, new PlaylistVidLookup(0, v.Id, pl.Id));
                    //pl = BuildList(pl);
                    //RefreshLists(false, pl, v);
                    pl.Videos.Add(v);
                    RefreshLists(false, pl);
                    Busy = false;
                    return;
                }
            }
            else if (await GetVideo(GrabFolder, pl, url, v, progress, CookieJar))
            {
                UpdateVideoData(db, v);
                InsertListVideoLookup(db, new PlaylistVidLookup(0, v.Id, pl.Id));
                //pl.Videos.Add(v);
                //pl.Videos = pl.Videos.OrderByDescending(k => k.UploadedDT).ThenByDescending(k => k.TimeDownloadedDT).ToList();
                //if (pl.NumVideos < pl.Videos.Count) pl.NumVideos = pl.Videos.Count;
                //pl = BuildList(pl);
                //RefreshLists(true, pl);
                pl.Videos.Add(v);
                RefreshLists(false, pl);
            }
            else if (cancelled) { pl.Status = string.Empty; Busy = false; return; }
            Busy = false;
        }
        private void ExportDatabase()
        {
            try
            {
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog
                {
                    Title = "Select destination for export",
                    DefaultExt = ".db",
                    Filter = "Database Files (.db)|*.db",
                    FileName = "grabtube.db"
                };
                Nullable<bool> result = dlg.ShowDialog();
                if (result == true)
                {
                    string filename = dlg.FileName;
                    File.Copy(GrabDbPath, filename);
                    MessageBox.Show(string.Format("video metadata exported"));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private Playlist EnsureList(Playlist template)
        {
            Playlist pl = Lists.Where(k => k.Type == template.Type && k.Name == template.Name).FirstOrDefault();
            //var lists = GetLists(db, " where type=" + template.Type + " and name='" + template.Name + "'");
            if (pl == null)
            {
                pl = UpdateList(template);
                //RefreshLists(true);
                SyncLists(null, pl, null, null);
                if (pl.Videos == null)
                {
                    pl.Videos = new List<Vid>();
                }
                return pl;
            }
            else
            {
                if (pl.Videos == null)
                {
                    pl.Videos = new List<Vid>();
                }
                return pl;
            }
        }
        private readonly ConcurrentBag<IDisposable> Disposables = new ConcurrentBag<IDisposable>();
        private void Login()
        {
            bool synchronous = true;
            string msg1 = @"A browser window will appear.  Login and ensure you're logged in.  This process is necessary to access private lists and videos.
After Login is complete close the browser window.    To forego this process and stay logged out close the browser window and your private lists will be unavailable.  You can log in/out again later via the login menu item.";
            if (!synchronous)
            {
                msg1 += "  After 100 seconds the browser will be forced to close.";
                MessageBox.Show(win, msg1);
                (new Thread(() =>
                {
                    IE ie = new IE(Constants.subscriptionsUrl, Constants.browserInProc);
                    Disposables.Add(ie);
                })
                { ApartmentState = ApartmentState.STA }).Start();
            }
            else
            {
                MessageBox.Show(win, msg1);
                using (IE br = new IE(Constants.subscriptionsUrl, Constants.browserInProc))
                {
                    try
                    {
                        while (true)
                        {
                            IntPtr r = br.hWnd;
                            Thread.Sleep(100);
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }
        internal Playlist UpdateList(Playlist lis = null)
        {
            Playlist t = null;
            if (lis != null)
            {
                t = lis;
            }
            else
            {
                string txt = "";
                try { txt = Clipboard.GetText(); } catch { }
                if (!string.IsNullOrWhiteSpace(txt))
                {
                    if (txt.StartsWith("http") && txt.Contains("/playlist?list=")) { } else { txt = null; }
                }
                else
                {
                    txt = null;
                }
                StringInputWindow siw = new StringInputWindow(new StringInputWindowPresenter.StringInputWindowData() { Title = "Enter List URL", ButtonText = "OK", Input = txt });
                bool? res = siw.ShowDialog(win);
                if (!res.HasValue || res.HasValue && !res.Value)
                {
                    return null;
                }
                t = new Playlist() { Url = siw.ViewModel.Data.Input, Grab = true };
            }
            int id = t.Id;
            string sql = string.Empty;
            bool insert = false;
            if (id > -1)
            {
                sql = string.Format(@"update list SET url = '{0}',name='{1}',vidcount={2},thumb_url='{3}', grab={5}, type={6} where id = {4}",
                    t.Url.Replace("'", "''"), t.Name.Replace("'", "''"), t.NumVideos, t.UrlThumb.Replace("'", "''"), id, t.Grab.ToBinaryInt(), (int)t.Type);
            }
            else
            {
                sql = string.Format(@"insert into list (url,name,vidcount,thumb_url,grab,type) values ('{0}','{1}','{2}','{3}',{4},{5})",
                    t.Url.Replace("'", "''"), t.Name.Replace("'", "''"), t.NumVideos, t.UrlThumb.Replace("'", "''"), t.Grab.ToBinaryInt(), (int)t.Type);
                insert = true;
            }
            SQLiteCommand command = new SQLiteCommand(sql, db);
            command.ExecuteNonQuery();
            if (insert)
            {
                t.Id = GetLastInsertedId(db);
            }
            SyncLists(null, t, null, null);
            NotifyPropertyChanged("Lists");
            return t;
        }
        private delegate void SyncListsDelegate(List<Playlist> addMany, Playlist addSingle, List<Playlist> removeMany, Playlist removeSingle);
        private void SyncLists(List<Playlist> addMany, Playlist addSingle, List<Playlist> removeMany, Playlist removeSingle)
        {
            if (InvokeIfNecessary(new SyncListsDelegate(SyncLists), addMany, addSingle, removeMany, removeSingle))
            {
                return;
            }
            List<Playlist> add = new List<Playlist>();
            if (addMany != null)
            {
                add.AddRange(addMany);
            }
            if (addSingle != null)
            {
                add.Add(addSingle);
            }
            List<Playlist> remove = new List<Playlist>();
            if (removeMany != null)
            {
                remove.AddRange(removeMany);
            }
            if (removeSingle != null)
            {
                remove.Add(removeSingle);
            }
            object sel = win.lvVideos.SelectedItem;
            object lsel = win.lvLists.SelectedItem;
            if (remove.Contains(lsel))
            {
                lsel = null;
            }
            IEnumerable<int> removeIds = remove.Select(k => k.Id);
            List<Playlist> toRemove = Lists.Where(k => removeIds.Contains(k.Id)).ToList();
            toRemove.ForEach(k => Lists.Remove(k));
            if (toRemove.Count > 0)
            {
                NotifyPropertyChanged("Lists");
            }
            add.ForEach(k =>
            {
                int i = Lists.IndexOf(k);
                if (i > -1)
                {
                    Lists[i] = k;
                    NotifyPropertyChanged("Lists[" + i + "]");
                }
                else
                {
                    Lists.Add(k);
                    NotifyPropertyChanged("Lists[" + Lists.IndexOf(k) + "]");
                }
            });
            if (win.lvLists.SelectedItem != lsel)
            {
                win.lvLists.SelectedItem = lsel;
            }
            if (win.lvVideos.SelectedItem != sel)
            {
                win.lvVideos.SelectedItem = sel;
            }
        }
        internal ObservableCollection<Playlist> Lists { get; set; }
        private Playlist GetList(string listUrl)
        {
            List<Playlist> lists = GetLists(db, string.Format(" where url like '%{0}%'", listUrl));
            if (lists.Count > 0)
            {
                return lists[0];
            }
            return null;
        }
        private List<Playlist> GetLists(SQLiteConnection db, string where = "")
        {
            List<PlaylistVidLookup> lookups = GetPlaylistVidLookups(db);
            List<Vid> vids = GetVideos(db);
            List<Playlist> Lists = new List<Playlist>();
            string sql = string.Format("select * from list " + where);
            SQLiteCommand command = new SQLiteCommand(sql, db);
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Playlist list = new Playlist
                {
                    Id = reader.SafeGetInt("id"),
                    Url = reader.SafeGetString("url"),
                    NumVideos = reader.SafeGetInt("vidcount"),
                    UrlThumb = reader.SafeGetString("thumb_url"),
                    Name = reader.SafeGetString("name"),
                    Grab = (reader.SafeGetInt("grab") == 1),
                    Type = (PlaylistType)reader.SafeGetInt("type")
                };
                List<int> mylooks = lookups.Where(k => k.ListId == list.Id).Select(k => k.VideoId).ToList();
                list.Videos = vids.Where(k => mylooks.Contains(k.Id)).OrderByDescending(k => k.UploadedDT).ThenByDescending(k => k.TimeDownloadedDT).ToList();
                Lists.Add(list);
            }
            Lists.ForEach(k => { if (k.Videos.Count > k.NumVideos) { k.NumVideos = k.Videos.Count; } });
            return Lists;
        }
        private void DeleteList()
        {
            object objItem = win.lvLists.SelectedItem;
            if (objItem == null)
            {
                return;
            }
            Playlist item = (Playlist)objItem;
            if (MessageBox.Show(win, $"Are you sure you want to delete the playlist \"{item.Name}\"", "WARNING", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                string sql = string.Format("delete from list where id = {0}", item.Id);
                SQLiteCommand command = new SQLiteCommand(sql, db);
                command.ExecuteNonQuery();
                SyncLists(null, null, null, item);
                NotifyPropertyChanged("Lists");
            }
        }
        private List<Playlist> FindPlaylists()
        {
            Busy = true;
            cancelled = false;
            List<Playlist> result = new List<Playlist>();
            List<Playlist> foundPlaylists = null;
            if (GetPlaylists2(ref foundPlaylists) == GetLibraryUrlResult.FoundLink)
            {
                List<Playlist> mainplaylists = Lists.Where(k => k.Type == PlaylistType.Normal).ToList();
                List<Playlist> addThese = new List<Playlist>();
                foreach (Playlist foundPl in foundPlaylists)
                {
                    Playlist npl = mainplaylists.FirstOrDefault(k => k.Url.Contains(foundPl.Url));
                    if (npl != null)
                    {
                        npl.Name = foundPl.Name;
                        npl.UrlThumb = foundPl.UrlThumb;
                    }
                    else
                    {
                        addThese.Add(foundPl);
                    }
                }
                result.AddRange(mainplaylists);
                result.AddRange(addThese);
            }
            Busy = false;
            return result;
        }
        private void UpdateLibraryAndLists()
        {
            new Thread(() =>
            {
                List<Playlist> found = FindPlaylists();
                if (found != null)
                {
                    found.Where(k => !Lists.Any(r => r.Url == k.Url)).ToList().ForEach(k =>
                    {
                        Playlist oldList = GetList(k.Url);
                        if (oldList != null)
                        {
                            k.Id = oldList.Id;
                        }
                        UpdateList(k);
                    });
                    if (!Lists.Any(k => k.Url == Constants.urlHistory))
                    {
                        UpdateList(new Playlist() { Name = "History", Url = Constants.urlHistory, Grab = false, Videos = new List<Vid>() });
                    }
                    if (!Lists.Any(k => k.Url == Constants.urlWatchLater))
                    {
                        UpdateList(new Playlist() { Name = "Watch later", Url = Constants.urlWatchLater, Grab = true, Videos = new List<Vid>() });
                    }
                    if (!Lists.Any(k => k.Url == Constants.urlLikedList))
                    {
                        UpdateList(new Playlist() { Name = "Liked Videos", Url = Constants.urlLikedList, Grab = false, Videos = new List<Vid>() });
                    }
                }
            }
             )
            { ApartmentState = ApartmentState.STA }.Start();
        }
        private void Start()
        {
            if (Busy)
            {
                return;
            } (new Thread(() => { Grab(); }) { ApartmentState = ApartmentState.STA }).Start();
        }
        private delegate bool BoolVidYoutubePlaylistStringDoubleDelegate(Vid vid, Playlist pl, string m, double k);
        private delegate void voidBoolYoutubePlaylistVidDelegate(bool r, Playlist list, Vid vid);
        private void About()
        {
            AboutWindow aw = new AboutWindow();
            aw.Show(win);
        }
        private void RefreshLists(bool getLists = false, Playlist list = null, Vid vid = null)
        {
            try
            {
                if (InvokeAsyncIfNecessary(new voidBoolYoutubePlaylistVidDelegate(RefreshLists), getLists, list, vid))
                {
                    return;
                }
                if (getLists)
                {
                    SyncLists(GetLists(db), list, null, null);
                }
                object sel = win.lvVideos.SelectedItem;
                object lsel = win.lvLists.SelectedItem;
                ListsView.Refresh();
                if (SelectedPlaylist != null)
                {
                    ICollectionView view2 = CollectionViewSource.GetDefaultView(SelectedPlaylist.Videos);
                    view2.Refresh();
                }
                if (vid != null && !getLists)
                {
                    if (SelectedPlaylist != null)
                    {
                        string r = "SelectedPlaylist.Videos[" + SelectedPlaylist.Videos.IndexOf(vid) + "]";
                        NotifyPropertyChanged(r);
                    }
                }
                else
                {
                    NotifyPropertyChanged("SelectedPlaylist.Videos");
                }
                if (list != null && !getLists)
                {
                    string r = "Lists[" + Lists.IndexOf(list) + "]";
                    NotifyPropertyChanged(r);
                }
                else
                {
                    NotifyPropertyChanged("Lists");
                }
                ListView lv = win.lvLists;
                if (win.lvLists.SelectedItem != lsel)
                {
                    win.lvLists.SelectedItem = lsel;
                }
                if (win.lvVideos.SelectedItem != sel)
                {
                    win.lvVideos.SelectedItem = sel;
                }
            }
            catch (Exception) { }
        }
        private delegate void voidDelegate();
        private delegate void voidUpdateDelegate(Update d);
        private void Tick(Update last = null)
        {
            if (last == null && (DateTime.Now - latestUpdate > new TimeSpan(0, 0, 2) || updates.Count < 1))
            {
                return;
            }
            if (last == null)
            {
                if (updates.TryPop(out Update g))
                {
                    updates.Clear();
                    if (InvokeAsyncIfNecessary(new voidUpdateDelegate(Tick), g))
                    {
                        return;
                    }
                }
            }
            if (last.d == 100)
            {
                last.pl.Status = string.Empty;
            }
            else
            {
                if (last.d == 0)
                {
                    last.pl.Status = string.Format("{0}", last.blurb);
                }
                else
                {
                    last.pl.Status = string.Format("{0:F1}% {1}", last.d, last.blurb);
                }
            }
            SyncLists(null, last.pl, null, null);
        }
        private readonly ConcurrentStack<Update> updates = new ConcurrentStack<Update>();
        private DateTime latestUpdate = DateTime.MinValue;
        private Timer updateTimer = null;
        private bool UpdateProgress(Vid vid, Playlist pl, string blurb, double d)
        {
            updates.Push(new Update() { vid = vid, pl = pl, d = d, blurb = blurb });
            latestUpdate = DateTime.Now;
            if (cancelled)
            {
                return false;
            }
            return (!closed);
        }
        public bool Busy { get => _Busy; set { _Busy = value; NotifyPropertyChanged("Busy"); } }
        private bool _Busy = false;
        private void Cancel()
        {
            cancelled = true;
            FeedWatchCanceller.Set();
            NotifyPropertyChanged("cancelled");
        }
        private bool cancelled = false;
        private async Task Grab()
        {
            Busy = true;
            cancelled = false;
            try
            {
                List<Playlist> listsGrab = new List<Playlist>();
                listsGrab.AddRange(Lists.Where(k => k.Name != "Liked videos" && k.Name != "History" && !k.Url.EndsWith("/playlists")).ToList());
                listsGrab = listsGrab.OrderBy(k => k.NumVideos).ToList();
                listsGrab.AddRange(Lists.Where(k => k.Name == "Liked videos" || k.Name == "History").ToList());
                listsGrab = listsGrab.Where(k => k.Grab && k.Type == PlaylistType.Normal).ToList();
                foreach (Playlist pl in listsGrab)
                {
                    if (cancelled) { pl.Status = string.Empty; Busy = false; return; }
                    await GrabList(pl);
                }
            }
            catch (Exception)
            {
            }
            RefreshLists(true);
            Busy = false;
            return;
        }
        private List<string> fileCache = new List<string>();
        private async Task GrabList(Playlist pl, int pageLimit = -1, GotVid gotVid = null, bool checkForLocalFile = true, ConfirmGetVid shouldGet = null)
        {
            Func<string, double, bool> progress2 = (t, d) => { return UpdateProgress(null, pl, t, d); };
            string urlPl = GetPlaylistUrlFromId(pl.Url);
            if (urlPl.EndsWith("="))
            {
                urlPl = pl.Url;
            }
            Console.WriteLine(pl.ToString());
            DateTime GrabVideoIdsTime = DateTime.Now;
            List<VideoId> vids = GetVideoIds(urlPl, ref cancelled, progress2, $"found {{0}} pages of links", pageLimit);
            if (cancelled) { pl.Status = string.Empty; Busy = false; return; }
            while (vids == null)
            {
                GrabVideoIdsTime = DateTime.Now;
                vids = GetVideoIds(urlPl, ref cancelled, progress2, $"found {{0}} pages of links", pageLimit);
                if (cancelled) { pl.Status = string.Empty; Busy = false; return; }
                Thread.Sleep(60000);
            }
            vids = vids.Distinct().ToList();
            if (pl.NumVideos != vids.Count) { pl.NumVideos = vids.Count; UpdateList(pl); }
            List<Tuple<Vid, VideoId>> pending = new List<Tuple<Vid, VideoId>>();
            int cacheCheckN = vids.Count;
            int cacheCheckI = 0;
            vids.ForEach(async videoId =>
            {
                cacheCheckI++;
                if (cacheCheckI % 10 == 0)
                { UpdateProgress(null, pl, "Checking grab folder for " + videoId.url, 100d * (cacheCheckI / (double)cacheCheckN)); }
                if (cancelled) { pl.Status = string.Empty; Busy = false; return; }
                Vid v = GetExistingData(videoId.url, db);
                if (string.IsNullOrWhiteSpace(v.Url))
                {
                    v.Url = GetVideoUrlFromId(videoId.url);
                }
                List<Vid> cached = VideoDownloaded(videoId.url, db, checkForLocalFile);
                List<string> existings = null;
                existings = GetVideoRelatedFiles(GrabFolder, v.Url, ref fileCache);
                bool existingVideoFile = existings.Where(k => IsVideo(k)).Count() > 0;
                Vid firstCached = cached.FirstOrDefault();
                if (Settings.FetchMissingVideoFiles && !existingVideoFile)
                {
                    pending.Add(new Tuple<Vid, VideoId>(v, videoId));
                }
                else if (firstCached == null && existingVideoFile)
                {
                    //TODO:
                    //Fetch metadata from YT
                    //Fallback to derive from filename
                }
                else if (firstCached == null)
                {
                    pending.Add(new Tuple<Vid, VideoId>(v, videoId));
                }
                else if (firstCached != null)
                {
                    List<PlaylistVidLookup> lookups = GetPlaylistVidLookups(db, $" where list_id = {pl.Id} AND video_id = {firstCached.Id}");
                    if (lookups.Count < 1)
                    {
                        InsertListVideoLookup(db, new PlaylistVidLookup(0, firstCached.Id, pl.Id));
                        pl.Videos.Add(firstCached);
                        pl.Videos = pl.Videos.OrderByDescending(k => k.UploadedDT).ThenByDescending(k => k.TimeDownloadedDT).ToList();
                        RefreshLists(false, pl);
                    }
                }
                //TODO: reimplement cross library linking
            });
            UpdateProgress(null, pl, null, 100);
            Console.WriteLine("{0}, Pending: {1}", pl.ToString(), pending.Count);
            bool gotVidKeepGoing = true;
            pending = pending.GroupBy(k => k.Item2.UploadedTimeOffset).SelectMany(k => k.Reverse()).ToList();
            int remaining = 0;
            foreach (Tuple<Vid, VideoId> u in pending)
            {
                if (shouldGet != null)
                {
                    if (!shouldGet(u.Item2))
                    {
                        return;
                    }
                }
                if (!gotVidKeepGoing) { return; }
                if (cancelled) { pl.Status = string.Empty; Busy = false; return; }
                Func<string, double, bool> progress = (t, d) => { return UpdateProgress(u.Item1, pl, $"[ {remaining}/{pending.Count} ] {t}", d); };
                Vid p = u.Item1;
                if (await GetVideo(GrabFolder, pl, u.Item1.Url, p, progress, CookieJar))
                {
                    remaining++;
                    fileCache.Add(p.FilePath);
                    if (u.Item2.UploadedTimeOffset != TimeSpan.MaxValue)
                    {
                        p.UploadedDT = (GrabVideoIdsTime - u.Item2.UploadedTimeOffset);
                        SetTimesOnFiles(p);
                    }
                    else
                    {
                        SetTimesOnFiles(p);
                    }
                    UpdateVideoData(db, p);
                    InsertListVideoLookup(db, new PlaylistVidLookup(0, p.Id, pl.Id));
                    pl.Videos.Add(p);
                    pl.Videos = pl.Videos.OrderByDescending(k => k.UploadedDT).ThenByDescending(k => k.TimeDownloadedDT).ToList();
                    if (pl.NumVideos < pl.Videos.Count)
                    {
                        pl.NumVideos = pl.Videos.Count;
                    }
                    RefreshLists(false, pl);
                    if (gotVid != null)
                    {
                        gotVidKeepGoing = gotVid(p);
                    }
                }
                else
                    if (cancelled) { pl.Status = string.Empty; Busy = false; return; }
            };
        }
        private delegate bool ConfirmGetVid(VideoId vidid);
        private delegate bool GotVid(Vid vid);
        private void FeedWatchStart()
        {
            (new Thread((r) => { FeedWatch(); }) { ApartmentState = ApartmentState.STA, Priority = ThreadPriority.BelowNormal }).Start();
        }
        private void FeedWatchServerStart()
        {
            (new Thread((r) => { FeedWatchServer(); }) { ApartmentState = ApartmentState.STA, Priority = ThreadPriority.BelowNormal }).Start();
        }
        private async void FeedWatchServer()
        {
            Busy = true;
            cancelled = false;
            Playlist pl = EnsureList(new Playlist() { Grab = false, Name = "subscriptions", Type = PlaylistType.Subscriptions, Url = Constants.subscriptionsUrl });
            using (IE ie = new IE(Constants.showBrowser))
            {
                ie.Visible = false;
                using (Server server = new Server())
                {
                    DateTime tim = DateTime.Now - new TimeSpan(100, 0, 0);
                    TimeSpan goodround = new TimeSpan(0, 10, 0);
                    TimeSpan goodness = TimeSpan.Zero;
                    TimeSpan futureGoodness = TimeSpan.Zero;
                    server.Listen(Settings, pl);
                    while (true)
                    {
                        if (cancelled) { pl.Status = string.Empty; Busy = false; return; }
                        DateTime timeVidListGrabbed = DateTime.Now;
                        ConfirmGetVid decider = new ConfirmGetVid((vidid) =>
                        {
                            DateTime vidTimeStamp = (vidid.UploadedTimeOffset != TimeSpan.MaxValue) ? timeVidListGrabbed - vidid.UploadedTimeOffset : timeVidListGrabbed;
                            //futureGoodness = new TimeSpan(0, 0, pl.Videos.Where(k => !k.Watched).Sum(k => k.Length));
                            List<Vid> vidGraph1 = pl.Videos.Where(k => !k.Streamed).ToList();
                            vidGraph1.Add(new Vid() { Url = vidid.url, UploadedDT = vidTimeStamp });
                            vidGraph1 = vidGraph1.OrderByDescending(k => k.UploadedDT).ToList();
                            TimeSpan timeIn = TimeSpan.Zero;
                            foreach (Vid r in vidGraph1)
                            {
                                if (r.Url.Contains(vidid.url))
                                {
                                    break;
                                }
                                timeIn = timeIn.Add(r.LengthTS);
                            }
                            //if (futureGoodness < goodround)
                            //    return true;
                            if (timeIn < goodround)
                            {
                                return true;
                            }
                            return false;
                        });
                        timeVidListGrabbed = DateTime.Now;
                        await GrabList(pl, 1, new GotVid((vid) =>
                        {
                            return true;
                            //if (cancelled) { pl.Status = string.Empty; Busy = false; return false; }
                            //good++;
                            //goodness += vid.LengthTS;
                            //futureGoodness = new TimeSpan(0, 0, pl.Videos.Where(k => !k.Watched).Sum(k => k.Length));
                            //return (futureGoodness < goodround) ? true : false;
                        }), false, decider);
                        if (cancelled) { pl.Status = string.Empty; Busy = false; return; }
                        Thread.Sleep(1100);
                        updates.Push(new Update() { vid = null, pl = pl, d = 0, blurb = "Waiting for new videos." });
                        FeedWatchCanceller.WaitOne(30000);
                        pl.Status = string.Empty;
                        RefreshLists(false, pl);
                        goodness = TimeSpan.Zero;
                        futureGoodness = TimeSpan.Zero;
                        if (cancelled) { pl.Status = string.Empty; Busy = false; return; }
                    }
                }
            }
            ;
        }
        private void FeedWatch()
        {
            if (Constants.GetMPCPath() == null)
            {
                Message("To use FeedWatch Media Player Classic must be installed.  Please download and install MPC.");
                return;
            }
            Busy = true;
            cancelled = false;
            Playlist pl = EnsureList(new Playlist() { Grab = false, Name = "subscriptions", Type = PlaylistType.Subscriptions, Url = Constants.subscriptionsUrl });
            using (IE ie = new IE(Constants.showBrowser))
            {
                ie.Visible = false;
                using (Player player = new Player())
                {
                    (new Thread((r) =>
                    {
                        while (true)
                        {
                            if (!player.Playing)
                            {
                                Vid vid = pl.Videos.Where(k => !k.Watched).FirstOrDefault();
                                if (vid != null)
                                {
                                    player.Play(vid, Settings);
                                    vid.Watched = true;
                                    UpdateVideoData(db, vid);
                                    RefreshLists(false, SelectedPlaylist, vid);
                                }
                            }
                            Thread.Sleep(1000);
                            if (cancelled)
                            {
                                return;
                            }
                        }
                    })
                    { ApartmentState = ApartmentState.STA }).Start();
                    DateTime tim = DateTime.Now - new TimeSpan(100, 0, 0);
                    TimeSpan goodround = new TimeSpan(0, 10, 0);
                    TimeSpan goodness = TimeSpan.Zero;
                    TimeSpan futureGoodness = TimeSpan.Zero;
                    while (true)
                    {
                        if (cancelled) { pl.Status = string.Empty; Busy = false; return; }
                        DateTime timeVidListGrabbed = DateTime.Now;
                        ConfirmGetVid decider = new ConfirmGetVid((vidid) =>
                        {
                            DateTime vidTimeStamp = (vidid.UploadedTimeOffset != TimeSpan.MaxValue) ? timeVidListGrabbed - vidid.UploadedTimeOffset : timeVidListGrabbed;
                            //futureGoodness = new TimeSpan(0, 0, pl.Videos.Where(k => !k.Watched).Sum(k => k.Length));
                            List<Vid> vidGraph1 = pl.Videos.Where(k => !k.Watched).ToList();
                            vidGraph1.Add(new Vid() { Url = vidid.url, UploadedDT = vidTimeStamp });
                            vidGraph1 = vidGraph1.OrderByDescending(k => k.UploadedDT).ToList();
                            TimeSpan timeIn = TimeSpan.Zero;
                            foreach (Vid r in vidGraph1)
                            {
                                if (r.Url.Contains(vidid.url))
                                {
                                    break;
                                }
                                timeIn = timeIn.Add(r.LengthTS);
                            }
                            //if (futureGoodness < goodround)
                            //    return true;
                            if (timeIn < goodround)
                            {
                                return true;
                            }
                            return false;
                        });
                        timeVidListGrabbed = DateTime.Now;
                        GrabList(pl, 1, new GotVid((vid) =>
                        {
                            return true;
                            //if (cancelled) { pl.Status = string.Empty; Busy = false; return false; }
                            //good++;
                            //goodness += vid.LengthTS;
                            //futureGoodness = new TimeSpan(0, 0, pl.Videos.Where(k => !k.Watched).Sum(k => k.Length));
                            //return (futureGoodness < goodround) ? true : false;
                        }), false, decider);
                        if (cancelled) { pl.Status = string.Empty; Busy = false; return; }
                        Thread.Sleep(1100);
                        updates.Push(new Update() { vid = null, pl = pl, d = 0, blurb = "Waiting for new videos." });
                        FeedWatchCanceller.WaitOne(30000);
                        pl.Status = string.Empty;
                        RefreshLists(false, pl);
                        goodness = TimeSpan.Zero;
                        futureGoodness = TimeSpan.Zero;
                        if (cancelled) { pl.Status = string.Empty; Busy = false; return; }
                    }
                }
            }
        }
        private readonly AutoResetEvent FeedWatchCanceller = new AutoResetEvent(false);
        private void Options()
        {
            OptionsWindow ow = new OptionsWindow();
            ow.Show(win);
        }
        public Options Settings { get; } = new Options();
        internal void SetSelectedPlaylist(Playlist pl)
        {
            SelectedPlaylist = pl;
            NotifyPropertyChanged("SelectedPlaylist");
        }
        public Playlist SelectedPlaylist { get; private set; } = null;
        internal void SetSelectedVideo(Vid v)
        {
            SelectedVideo = v;
            if (SelectedVideo != null && !string.IsNullOrWhiteSpace(SelectedVideo.Description))
            {
                string ht = SelectedVideo.Description;
                string xa = HtmlToXamlConverter.ConvertHtmlToXaml(ht, true).Replace(" xml:space=\"preserve\" ", " ");
                FlowDocument doc = null;
                try
                {
                    doc = XamlReader.Parse(xa) as FlowDocument;
                    SubscribeToAllHyperlinks(doc);
                }
                catch
                {
                }
                if (doc == null)
                {
                    try
                    {
                        doc = new FlowDocument();
                        Paragraph p = new Paragraph(new Run(HtmlToText.ConvertHtml(ht)))
                        {
                            FontSize = 14
                        };
                        doc.Blocks.Add(p);
                    }
                    catch
                    {
                        doc = new FlowDocument();
                        Paragraph p = new Paragraph(new Run(ht));
                    }
                }
                win.fdrDescription.Document = doc;
            }
            else
            {
                win.fdrDescription.Document = null;
            }
            NotifyPropertyChanged("SelectedVideo");
        }
        private void SubscribeToAllHyperlinks(FlowDocument flowDocument)
        {
            //http://stackoverflow.com/questions/5465667/handle-all-hyperlinks-mouseenter-event-in-a-loaded-loose-flowdocument
            IEnumerable<Hyperlink> hyperlinks = GetVisuals(flowDocument).OfType<Hyperlink>();
            foreach (Hyperlink link in hyperlinks)
            {
                link.RequestNavigate += new System.Windows.Navigation.RequestNavigateEventHandler(Link_RequestNavigate);
            }
        }
        private void Link_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            //http://stackoverflow.com/questions/2288999/how-can-i-get-a-flowdocument-hyperlink-to-launch-browser-and-go-to-url-in-a-wpf
            string url = e.Uri.OriginalString;
            if (url.StartsWith("/"))
            {
                url = Constants.urlTube + url;
            }
            Process.Start(new ProcessStartInfo(url));
            e.Handled = true;
        }
        public Vid SelectedVideo { get; set; } = null;
        internal void ShareVideo(Vid vid)
        {
            try
            {
                string t = GetVideoUrlFromId(vid.Url);
                Clipboard.SetText(t, TextDataFormat.Text);
                MessageBox.Show(win, "URL copied to clipboard.");
            }
            catch { }
        }
        internal void GoToPage(Vid vid)
        {
            Process.Start(new ProcessStartInfo(GetVideoUrlFromId(vid.Url)));
        }
        internal void Watch(Vid vid)
        {
            if (File.Exists(vid.FilePath))
            {
                try
                {
                    string app = FileAssociation.GetExecFileAssociatedToExtension(Path.GetExtension(vid.FilePath), null);
                    if (app != null)
                    {
                        vid.Watched = true;
                        UpdateVideoData(db, vid);
                        RefreshLists(false, SelectedPlaylist, vid);
                        Process process = new Process();
                        process.StartInfo.FileName = app;
                        process.StartInfo.Arguments = $@"""{vid.FilePath}""";
                        process.StartInfo.WindowStyle = ProcessWindowStyle.Maximized;
                        process.Start();
                    }
                }
                catch (Exception)
                {
                }
            }
        }
        private MessageBoxResult Ask(string title, string body)
        {
            object res = MessageBoxResult.No;
            Func<MessageBoxResult> func = new Func<MessageBoxResult>(() => { return MessageBox.Show(win, title, body, MessageBoxButton.OKCancel, MessageBoxImage.Exclamation, MessageBoxResult.Cancel); });
            if (InvokeIfNecessaryWithRes(func, ref res))
            {
                return (MessageBoxResult)res;
            }
            return func();
        }
        public static void BrowserShutdown()
        {
            try
            {
                Global.RunBrowserAction((ie2) =>
                {
                    Process ie = Process.GetProcessById(ie2.ProcessID);
                    try { ie2.ForceClose(); } finally { }
                    ie.WaitForExit(1000);
                    try { ie2.Dispose(); } finally { }
                    ie.WaitForExit(1000);
                    try { ie.Kill(); } finally { }
                    Global.EndBrowserWork();
                });
            }
            catch
            {
            }
        }
        public static IEnumerable<DependencyObject> GetVisuals(DependencyObject root)
        {
            foreach (DependencyObject child in LogicalTreeHelper.GetChildren(root).OfType<DependencyObject>())
            {
                yield return child;
                foreach (DependencyObject descendants in GetVisuals(child))
                {
                    yield return descendants;
                }
            }
        }
        public static ImageSource GetPic(string filPth)
        {
            if (!File.Exists(filPth))
            {
                return null;
            }
            try
            {
                BitmapSource bs = new BitmapImage(new Uri(filPth, UriKind.Absolute));
                return bs;
            }
            catch
            {
                return null;
            }
        }
        public static string Path_data
        {
            get
            {
                string pth = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), Constants.appName);
                if (!Directory.Exists(pth))
                {
                    try
                    {
                        Directory.CreateDirectory(pth);
                    }
                    catch { return null; }
                }
                return pth;
            }
        }
        public static string Path_entropy => Path.Combine(Path_data, ".entropy");
        public static byte[] Entropy
        {
            get
            {
                byte[] ent;
                if (File.Exists(Path_entropy))
                {
                    ent = File.ReadAllBytes(Path_entropy);
                }
                else
                {
                    ent = new byte[32];
                    rand.NextBytes(ent);
                    File.WriteAllBytes(Path_entropy, ent);
                }
                return ent;
            }
        }
        public static Random rand = new Random();
        public static string MakeValidFileName(Vid vid)
        {
            return MakeVideoFileNameSuggestion(vid.Author, vid.Title, GetVideoIdFromUrl(vid.Url));
        }
        public static string MakeValidFileName(Video vid)
        {
            return MakeVideoFileNameSuggestion(vid.Author, vid.Title, GetVideoIdFromUrl(vid.Url));
        }
        public static string MakeValidFileName(string name)
        {
            string t = "";
            foreach (char c in name)
            {
                if (Constants.validFileChars.Contains(c))
                {
                    t += c;
                }
                else
                {
                    t += "_";
                }
            }
            t = t.TrimEnd('.');
            return t.Trim();
        }
        public static string MakeValidFileName(Video vid, string url)
        {
            return MakeVideoFileNameSuggestion(vid.Author, vid.Title, GetVideoIdFromUrl(url));
        }
        public static string MakeVideoFileNameSuggestion(string author, string title, string id)
        {
            return MakeValidFileName(string.Format("{0} - {1}", author, title)) + " [" + id + "]";
        }
        public static string GetThumbUrlFromId(string videoId, THUMB_QUALITY qual)
        {
            string t = videoId.Trim();
            return "http://img.youtube.com/vi/" + t + "/" + qual.ToString() + ".jpg";
        }
        public static string GetVideoIdFromUrl(string videoUrl)
        {
            string vidid = "";
            if (GetVideoIdFromUrl(videoUrl, ref vidid))
            {
                return vidid;
            }
            else
            {
                return null;
            }
        }
        public static bool GetVideoIdFromUrl(string videoUrl, ref string VideoId)
        {
            VideoId = null;
            string link2 = videoUrl.Trim();
            Match match = Regex.Match(link2, "\\/watch\\?v=([a-zA-Z0-9\\-_]+)");
            if (match.Success)
            { VideoId = match.Groups[1].Value; return true; }
            else
            {
                Match match2 = Regex.Match(link2, "v=([a-zA-Z0-9\\-_]+)");
                if (match2.Success)
                { VideoId = match2.Groups[1].Value; return true; }
                else
                {
                    //Match match3 = Regex.Match(link2, "/([a-zA-Z0-9-_]+)$");
                    //if (match3.Success)
                    //{ VideoId = match3.Groups[1].Value; return true; }
                    //else
                    //{
                    //    return false;
                    //}
                }
            }
            return false;
        }
        public static bool DownloadFileFromURL(string filePath, string url)
        {
            bool ret = false;
            if (!string.IsNullOrWhiteSpace(url) && !string.IsNullOrWhiteSpace(filePath))
            {
                try
                {
                    using (HttpHelper.CookieAwareWebClient client = BrowserShell.NewWebClient()) { client.DownloadFile(url, filePath); }
                    if (File.Exists(filePath))
                    {
                        ret = true;
                    }
                }
                catch { return false; }
            }
            return ret;
        }
        public static bool DownloadThumbnail(string filePath, string urlFallback, string vid)
        {
            bool ret = false;
            if (urlFallback != "")
            {
                string mcafe = System.IO.Path.GetExtension(urlFallback);
                try
                {
                    using (HttpHelper.CookieAwareWebClient client = BrowserShell.NewWebClient()) { client.DownloadFile(urlFallback, filePath); }
                    if (File.Exists(filePath))
                    {
                        ret = true;
                    }
                }
                catch { return false; }
            }
            if (!ret && !string.IsNullOrWhiteSpace(vid))
            {
                string urlThumb = GetThumbUrlFromId(vid, THUMB_QUALITY.hqdefault);
                try
                {
                    using (HttpHelper.CookieAwareWebClient client = BrowserShell.NewWebClient()) { client.DownloadFile(urlThumb, filePath); }
                    if (File.Exists(filePath))
                    {
                        ret = true;
                    }
                }
                catch { }
            }
            return ret;
        }
        /// <summary>
        /// Gets the audio extension.
        /// </summary>
        /// <value>The audio extension, or <c>null</c> if the audio extension is unknown.</value>
        public static string AudioExtension(AudioType info)
        {
            switch (info)
            {
                case AudioType.aac:
                    return ".aac";
                case AudioType.mp3:
                    return ".mp3";
                case AudioType.vorbis:
                    return ".ogg";
                case AudioType.opus:
                    return ".opus";
                case AudioType.mp4a:
                    return ".aac";
            }
            Debugger.Break();
            return null;
            //throw new Exception($"unknown audio type: {info}");
        }
        /// <summary>
        /// Gets the video extension.
        /// </summary>
        /// <value>The video extension, or <c>null</c> if the video extension is unknown.</value>
        public static string VideoExtension(VideoType type)
        {
            switch (type)
            {
                case VideoType.av1:
                    return ".mkv";
                case VideoType.h264:
                    return ".mp4";
                case VideoType.vp8:
                    return ".mkv";
                case VideoType.vp9:
                    return ".mkv";
                case VideoType.unknown:
                    return ".mkv";
                case VideoType.mp4:
                    return ".mp4";
                case VideoType.mobile:
                    return ".3gp";
                case VideoType.flash:
                    return ".flv";
                case VideoType.webm:
                    return ".webm";
            }
            return null;
        }
        /// <summary>
        /// Tests a file extension to see if it's a video file
        /// </summary>
        /// <value>The video extension, or <c>null</c> if the video extension is unknown.</value>
        public static bool IsVideo(string fExt)
        {
            fExt = Path.GetExtension(fExt).Replace(".", "").ToLower();
            bool itIs = (fExt == "mkv" || fExt == "mp4" || fExt == "3gp" || fExt == "flv" || fExt == "webm" || fExt == "avi");
            return itIs;
        }
        private static bool CanDl(string url, ref string output)
        {
            HttpWebRequest req = BrowserShell.NewWebRequest(url);
            try
            {
                using (WebResponse response = req.GetResponse())
                {
                    using (Stream source = response.GetResponseStream())
                    {
                        byte[] test = new byte[100];
                        return source.Read(test, 0, 100) > 0;
                    }
                }
            }
            catch (WebException wex)
            {
                //wex.
                if (wex.Status == WebExceptionStatus.ProtocolError)
                {
                    if (wex.Message.Contains("(403)"))
                    {
                        output += wex.ToString() + Environment.NewLine;
                        return false;
                    }
                }
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static async Task<DownloadResults> Download(string url, string savePath, Func<string, double, bool> progress, CookieContainer cookieJar, DownloadResults results)
        {
            ExplodingDownloader downloader = new ExplodingDownloader(BrowserShell.ExplodeClient)
            {
                Query = url
            };
            await downloader.PullData();
            Video metadata = downloader.Video;
            results.Info = metadata;
            IVideoStreamInfo bestVideo = downloader.VideoOnlyStreamInfos.Where(k => k.Size.TotalBytes > 65536).WithHighestVideoQuality();
            IEnumerable<VideoOnlyStreamInfo> besties = downloader.VideoOnlyStreamInfos.Where(k => k.Size.TotalBytes > 65536).Where(k => k.VideoQuality == bestVideo.VideoQuality);
            VideoOnlyStreamInfo zVp9 = besties.FirstOrDefault(k => k.VideoCodec == "vp9" && bestVideo.VideoCodec.StartsWith("avc1"));
            if (zVp9 != null)
            {
                bestVideo = zVp9;
            }
            VideoOnlyStreamInfo zAv1 = besties.FirstOrDefault(k => k.VideoCodec == "av1" && (bestVideo.VideoCodec.StartsWith("vp9") || bestVideo.VideoCodec.StartsWith("avc1")));
            if (zAv1 != null)
            {
                bestVideo = zAv1;
            }
            if (bestVideo != null && bestVideo.VideoCodec.Length > 2)
            {
                IEnumerable<VideoOnlyStreamInfo> besties60 = besties.Where(k => k.VideoCodec.Length > 2).Where(k => k.Framerate.FramesPerSecond > 59 && k.VideoCodec.StartsWith(bestVideo.VideoCodec.Substring(0, 3)));
                if (bestVideo.Framerate.FramesPerSecond < 60 && besties60.Count() > 0)
                {
                    VideoOnlyStreamInfo validUpgrade = besties60.FirstOrDefault(k => k.VideoQuality == bestVideo.VideoQuality);
                    if (validUpgrade != null)
                    {
                        bestVideo = validUpgrade;
                    }
                }
            }
            if (bestVideo == null)
            {
                //TODO: maybe try some others?
                results.Success = false;
                return results;
            }
            IStreamInfo bestAudio = downloader.AudioOnlyStreamInfos.WithHighestBitrate();
            if (bestAudio != null)
            {
                if (AudioExtension(RebuildMedia.GetAudioType((IAudioStreamInfo)bestAudio)) != ".aac" && VideoExtension(RebuildMedia.GetVideoType(bestVideo)) == ".mp4")
                {
                    AudioOnlyStreamInfo g = downloader.AudioOnlyStreamInfos.Where(k => k.AudioCodec == "aac").OrderBy(k => k.Bitrate).FirstOrDefault();
                    if (g != null)
                    {
                        bestAudio = g;
                    }
                }
            }
            if (bestAudio == null)
            {
                //TODO: maybe try some others?
                results.Success = false;
                return results;
            }
            RebuildMedia rm = new RebuildMedia
            {
                BestAud = (IAudioStreamInfo)bestAudio,
                BestVid = bestVideo
            };
            if (rm == null)
            {
                throw new Exception($"problem downloading {url}");
            }
            List<RebuildMedia.Pass> strat = rm.GetStrategy();
            //keep the video and convert the audio
            strat[0].KeepAudio = false;
            strat[1].KeepAudio = true;
            strat[0].ConvertVideo = false;
            strat[1].ConvertVideo = false;
            strat[0].ConvertAudio = false;
            strat[1].ConvertAudio = true;
            strat[0].KeepVideo = true;
            strat[1].KeepVideo = false;
            if (strat.Count > 1 && strat[0].KeepAudio)
            {
                throw new Exception("problem");
            }
            if (strat.Count > 1 && !strat[0].KeepAudio && !strat[1].KeepAudio)
            {
                throw new Exception("could not find audio download");
            }
            if (strat.Count > 1 && !strat[0].KeepVideo && !strat[1].KeepVideo)
            {
                throw new Exception("could not find video download");
            }
            if (strat.Count == 1 && !strat[0].KeepVideo)
            {
                throw new Exception("could not find video download");
            }
            if (strat.Count == 1 && !strat[0].KeepAudio)
            {
                throw new Exception("could not find audio download");
            }
            VideoType srcVidTyp = RebuildMedia.GetVideoType(strat[0].video);
            AudioType srcAudTyp = RebuildMedia.GetAudioType(strat[1].audio);
            string mediaTitle = MakeValidFileName(metadata, url);
            string fpVidSrc = System.IO.Path.Combine(savePath, mediaTitle + "vidsrc" + VideoExtension(srcVidTyp));
            results.FpVidSrc = fpVidSrc;
            string basePth = Path.Combine(savePath, mediaTitle);
            string vidPath = fpVidSrc;
            string vidExtr = basePth + "extractedVid" + VideoExtension(srcVidTyp);
            string vidCvt = basePth + "convertedVid.mp4";
            string audExtr = basePth + "extractedAud" + AudioExtension(srcAudTyp);
            bool rAudCvtToAAC = (VideoExtension(srcVidTyp) == ".mp4" && AudioExtension(srcAudTyp) != ".aac");
            bool rAudCvtFromAAC = (VideoExtension(srcVidTyp) != ".mp4" && AudioExtension(srcAudTyp) == ".aac");
            string rAudExt = rAudCvtToAAC ? ".aac" : AudioExtension(srcAudTyp);
            string audCvt = $"{basePth}extractedConvertedAud{rAudExt}";
            strat[1].ConvertAudio = rAudCvtToAAC;
            string cap = basePth + ".vtt";
            string capSrt = basePth + ".srt";
            string fnlExt = VideoExtension(srcVidTyp);
            string comb = $"{basePth}combined{fnlExt}";
            string fnl = $"{basePth}{fnlExt}";
            string vid = "";
            string fnlCC = null;
            if (File.Exists(vidExtr))
            {
                File.Delete(vidExtr);
            }
            if (File.Exists(vidCvt))
            {
                File.Delete(vidCvt);
            }
            if (File.Exists(audExtr))
            {
                File.Delete(audExtr);
            }
            if (File.Exists(audCvt))
            {
                File.Delete(audCvt);
            }
            if (File.Exists(comb))
            {
                File.Delete(comb);
            }
            if (!GetVideoIdFromUrl(url, ref vid))
            {
                vid = url;
            }
            string[] thumbs = new string[] {
                 metadata.Thumbnails.MaxResUrl,
                 metadata.Thumbnails.HighResUrl,
                 metadata.Thumbnails.MediumResUrl,
                 metadata.Thumbnails.StandardResUrl,
                 metadata.Thumbnails.LowResUrl,
                };
            for (int i = 0; i < thumbs.Length; i++)
            {
                if (DownloadFileFromURL($"{basePth}.jpg", thumbs[i]))
                {
                    results.FpMediaCoverArt = $"{basePth}.jpg";
                    break;
                }
            }
            if (!File.Exists(capSrt))
            {
                if (downloader.ClosedCaptionTrackInfos != null && downloader.ClosedCaptionTrackInfos.Count > 0)
                {
                    IEnumerable<YoutubeExplode.Videos.ClosedCaptions.ClosedCaptionTrackInfo> primary = downloader.ClosedCaptionTrackInfos.Where(k => k.Language.Code.ToLower() == "en" && !k.IsAutoGenerated);
                    YoutubeExplode.Videos.ClosedCaptions.ClosedCaptionTrackInfo fallback = downloader.ClosedCaptionTrackInfos.Where(k => k.Language.Code.ToString() == "en" && k.IsAutoGenerated).FirstOrDefault();
                    List<string> tries = new List<string>();
                    tries.AddRange(primary.Select(k => k.Url));
                    tries.Add(fallback?.Url ?? null);
                    tries.RemoveAll(k => k == null);
                    for (int i = 0; i < tries.Count; i++)
                    {
                        if (!File.Exists(capSrt))
                        {
                            try
                            {
                                if (!File.Exists(cap))
                                {
                                    DownloadFileFromURL(cap, tries[i]);
                                }
                                if (File.Exists(cap))
                                {
                                    SRTConvert.ConvertTTMLToSRT(cap, capSrt);
                                    if (File.Exists(capSrt))
                                    {
                                        fnlCC = capSrt;
                                        break;
                                    }
                                }
                            }
                            catch { }
                        }
                        else if (!File.Exists(capSrt))
                        {
                            try
                            {
                                SRTConvert.ConvertTTMLToSRT(cap, capSrt);
                            }
                            catch { }
                        }
                    }
                }
            }
            else
            {
                fnlCC = capSrt;
            }
            if (File.Exists(fpVidSrc))
            {
                FileInfo fi = new FileInfo(fpVidSrc);
                if (fi.Length != bestVideo.Size.TotalBytes)
                {
                    fi.Delete();
                }
            }
            if (!File.Exists(fpVidSrc))
            {
                // Set up progress handler
                Progress<double> progressHandler = new Progress<double>(p => progress($"downloading {bestVideo} for {metadata}", p * 100));
                // Download to file
                await downloader.Client.Videos.Streams.DownloadAsync(bestVideo, fpVidSrc, progressHandler);
                if (!File.Exists(fpVidSrc))
                {
                    results.Success = false;
                    return results;
                }
                else if (bestVideo.Size.TotalBytes != new FileInfo(fpVidSrc).Length)
                {
                    File.Delete(fpVidSrc);
                    results.Success = false;
                    return results;
                }
            }
            string fpAudSrc = string.Empty;
            if (strat.Count > 1)
            {
                fpAudSrc = basePth + "audsrc" + AudioExtension(srcAudTyp);
                if (!File.Exists(fpAudSrc))
                {
                    // Set up progress handler
                    Progress<double> progressHandler2 = new Progress<double>(p => progress($"downloading {bestAudio} for {metadata}", p * 100));
                    // Download to file
                    await downloader.Client.Videos.Streams.DownloadAsync(bestAudio, fpAudSrc, progressHandler2);
                    if (!File.Exists(fpAudSrc))
                    {
                        results.Success = false;
                        return results;
                    }
                }
            }
            if (!FFMPEG(string.Format("-y -i \"{0}\" -vcodec copy -an \"{1}\"", fpVidSrc, vidExtr), (t, g) => { return progress("Extracting video: " + t, g); }))
            {
                throw new Exception("FFMPEG process failed.");
            }
            if (!File.Exists(vidExtr))
            {
                results.Success = false;
                return results;
            }
            rm.FinalVideo = vidExtr;
            if (strat.Count > 1)
            {
                audExtr = basePth + "extractedAud" + AudioExtension(srcAudTyp);
                if (!FFMPEG(string.Format("-y -i \"{0}\" -vn -acodec copy \"{1}\"", fpAudSrc, audExtr), (t, g) => { return progress("Extracting audio: " + t, g); }))
                {
                    throw new Exception("FFMPEG process failed.");
                }
                if (!File.Exists(audExtr))
                {
                    results.Success = false;
                    return results;
                }
                rm.FinalAudio = audExtr;
                if (strat[1].ConvertAudio)
                {
                    if (!rAudCvtFromAAC)
                    {
                        if (!FFMPEG(string.Format("-y -i \"{0}\" -vn -c:a aac -strict experimental \"{1}\"", audExtr, audCvt), (t, g) => { return progress("Converting audio: " + t, g); }))
                        {
                            throw new Exception("FFMPEG process failed.");
                        }
                        if (!File.Exists(audCvt))
                        {
                            results.Success = false;
                            return results;
                        }
                        rm.FinalAudio = audCvt;
                    }
                }
            }
            string movflags = (Path.GetExtension(rm.FinalVideo) == ".mp4") ? $" -movflags faststart" : "";
            string capIncl = !string.IsNullOrWhiteSpace(fnlCC) ? ((File.Exists(fnlCC)) ? $" -f srt -i \"{capSrt}\" -disposition:s:0 default" : "") : string.Empty;
            string audioCodec = "";
            if (!rAudCvtFromAAC)
            {
                audioCodec = "-c:a copy";
            }
            if (Path.GetExtension(rm.FinalVideo) == ".mp4")
            {
                movflags = "";
                if (Path.GetExtension(rm.FinalAudio) != ".aac")
                {
                    audioCodec = "-c:a aac";
                }
            }
            // finally, combine the streams
            if (!FFMPEG(string.Format($"-y -i \"{rm.FinalVideo}\" -i \"{rm.FinalAudio}\"{movflags}{capIncl} -c:v copy {audioCodec} \"{comb}\""), (t, g) => { return progress("Combining video and audio: " + t, g); }))
            {
                throw new Exception("FFMPEG process failed.");
            }
            //FFMPEG(string.Format($"-y -i \"{rm.FinalVideo}\" -i \"{rm.FinalAudio}\"{movflags}{capIncl} -c:v copy -c:a copy -bsf:a aac_adtstoasc -strict experimental \"{comb}\""), (t, g) => { return progress("Combining video and audio: " + t, g); });
            if (!File.Exists(comb))
            {
                results.Success = false;
                return results;
            }
            else if (new FileInfo(comb).Length < 65536)
            {
                File.Delete(comb);
                results.Success = false;
                return results;
            }
            if (File.Exists(vidExtr))
            {
                File.Delete(vidExtr);
            }
            if (File.Exists(vidCvt))
            {
                File.Delete(vidCvt);
            }
            if (File.Exists(audExtr))
            {
                File.Delete(audExtr);
            }
            if (File.Exists(audCvt))
            {
                File.Delete(audCvt);
            }
            if (File.Exists(fpAudSrc))
            {
                File.Delete(fpAudSrc);
            }
            if (File.Exists(fnl))
            {
                File.Delete(fnl);
            }
            if (File.Exists(fpVidSrc))
            {
                File.Delete(fpVidSrc);
            }
            File.Move(comb, fnl);
            fpVidSrc = fnl;
            progress("Finished!", 100);
            results.FpVidSrc = fpVidSrc;
            if (!string.IsNullOrWhiteSpace(fnlCC))
            {
                results.FpSrt = Path.Combine(Path.GetDirectoryName(fnlCC), Path.GetFileNameWithoutExtension(fnlCC) + ".srt");
                results.FpVtt = Path.Combine(Path.GetDirectoryName(fnlCC), Path.GetFileNameWithoutExtension(fnlCC) + ".vtt");
            }
            results.Success = true;
            return results;
        }
        public static string RunProcess(string workingDir, string fileName, string args, Func<string, double, bool> progress)
        {
            progress(string.Format("{0} {1}", fileName, args), 0);
            Process process = new Process();
            StringBuilder outputStringBuilder = new StringBuilder();
            try
            {
                process.StartInfo.WorkingDirectory = workingDir;
                process.StartInfo.FileName = fileName;
                process.StartInfo.Arguments = args;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.UseShellExecute = false;
                process.EnableRaisingEvents = false;
                process.OutputDataReceived += (sender, eventArgs) => outputStringBuilder.AppendLine(eventArgs.Data);
                process.ErrorDataReceived += (sender, eventArgs) => outputStringBuilder.AppendLine(eventArgs.Data);
                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();
                process.WaitForExit();
                process.CancelErrorRead();
                process.CancelOutputRead();
                if (process.ExitCode != 0)
                {
                    string output = outputStringBuilder.ToString();
                    throw new Exception("Process exited with non-zero exit code of: " + process.ExitCode + Environment.NewLine + "Output from process: " + outputStringBuilder.ToString());
                }
            }
            finally
            {
                process.Close();
            }
            progress(string.Format("{0} {1}", fileName, args), 100);
            return outputStringBuilder.ToString();
        }
        public static string DirPathFfmpeg()
        {
            return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase), "ffmpeg").Replace("file:\\", "");
        }
        public static bool FFMPEG(string args, Func<string, double, bool> progress)
        {
            try
            {
                Process process = new Process();
                process.StartInfo.WorkingDirectory = DirPathFfmpeg();
                process.StartInfo.FileName = "ffmpeg.exe";
                process.StartInfo.Arguments = args;
                progress(string.Format("{0} {1}", process.StartInfo.FileName, process.StartInfo.Arguments), 0);
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.UseShellExecute = false;
                Environment.CurrentDirectory = DirPathFfmpeg();
                process.Start();
                process.PriorityClass = ProcessPriorityClass.BelowNormal;
                Thread.Sleep(1000);
                process.WaitForExit();// Waits here for the process to exit.
                progress(string.Format("{0} {1}", process.StartInfo.FileName, process.StartInfo.Arguments), 100);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public static void VidTranscodeLow(string inf, string ouf, Func<string, double, bool> progress)
        {
            Process process = new Process();
            process.StartInfo.WorkingDirectory = Path.Combine(Environment.CurrentDirectory, "ffmpeg");
            // Configure the process using the StartInfo properties.
            process.StartInfo.FileName = "ffmpeg.exe";
            process.StartInfo.Arguments = string.Format("-i \"{0}\" -acodec copy -profile:v baseline -vcodec libx264 -level:v 1.2 -vf scale=256:144 \"{1}\"", inf, ouf);
            progress(string.Format("{0} {1}", process.StartInfo.FileName, process.StartInfo.Arguments), 0);
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.StartInfo.CreateNoWindow = true;
            process.Start();
            process.PriorityClass = ProcessPriorityClass.BelowNormal;
            process.WaitForExit();// Waits here for the process to exit.
            progress(string.Format("{0} {1}", process.StartInfo.FileName, process.StartInfo.Arguments), 100);
        }
        internal static string GetVideoUrlFromId(string videoId)
        {
            string t = videoId.Trim();
            if (t.StartsWith("https://") || t.StartsWith("http://"))
            {
                return videoId;
            }
            return Constants.urlWatchUrl + videoId;
        }
        internal static string GetPlaylistUrlFromId(string playlistId)
        {
            string t = playlistId.Trim();
            if (t.StartsWith("https://") || t.StartsWith("http://"))
            {
                return playlistId;
            }
            return Constants.urlViewPlUrl + playlistId;
        }
        internal static async Task<DownloadResults> GetVideo(string dataFolder, string playlistName, string videoId, Func<string, double, bool> progress, CookieContainer cookieJar, DownloadResults results)
        {
            string destFldr = DerivePath(dataFolder, playlistName);
            string videoUrl = GetVideoUrlFromId(videoId);
        startTry:
            if (!Directory.Exists(destFldr))
            {
                Directory.CreateDirectory(destFldr);
            }
            try
            {
                BrowserShell.Instance.FireDownloadAttemptOuter(videoId);
                results = await Download(GetVideoUrlFromId(videoId), destFldr, progress, cookieJar, results);
                return results;
            }
            catch (RequestLimitExceededException rlex)
            {
                BrowserShell.Instance.FireDownloadException(rlex);
                await Global.Instance.Backoff.Delay();
                goto startTry;
            }
            catch (TransientFailureException tfex)
            {
                BrowserShell.Instance.FireDownloadException(tfex);
                await Global.Instance.Backoff.Delay();
                goto startTry;
            }
            catch (Exception ex)
            {
                BrowserShell.Instance.FireDownloadException(ex);
                return new DownloadResults();
            }
        }
        internal static string DerivePath(string dataFolder, string playlistName)
        {
            return Path.Combine(dataFolder, playlistName);
        }
        internal static void ReLocate(SQLiteConnection db, string From, string To)
        {
            List<Vid> allvids = GetVideos(db);
            allvids.ToList().ForEach(k =>
            {
                if (k.FilePath.StartsWith(From) || k.FilePathThumb.StartsWith(From))
                {
                    if (k.FilePath.StartsWith(From))
                    {
                        k.FilePath = k.FilePath.Replace(From, To);
                    }
                    if (k.FilePathThumb.StartsWith(From))
                    {
                        k.FilePathThumb = k.FilePathThumb.Replace(From, To);
                    }
                    UpdateVideoData(db, k);
                }
            });
        }
        internal static string AppendToFilePath(string filePath, string appendage)
        {
            string fnwe = Path.GetFileNameWithoutExtension(filePath);
            string fldr = Path.GetDirectoryName(filePath);
            string ext = Path.GetExtension(filePath);
            return string.Format("{0}{1}{2}", Path.Combine(fldr, fnwe), appendage, ext);
        }
        internal static void BrandFilenames(SQLiteConnection db)
        {
            List<Vid> allvids = GetVideos(db);
            allvids.ToList().ForEach(k =>
            {
                string vidid = "";
                if (k.Url.StartsWith("http://") || k.Url.StartsWith("https://"))
                {
                    GetVideoIdFromUrl(k.Url, ref vidid);
                }
                else
                {
                    vidid = k.Url;
                }
                if (k.Downloaded)
                {
                    if (File.Exists(k.FilePath) && File.Exists(k.FilePathThumb))
                    {
                        bool g = false;
                        bool y = false;
                        if (!k.FilePath.Contains("[" + vidid + "]"))
                        {
                            string newFp = AppendToFilePath(k.FilePath, " [" + vidid + "]");
                            try { File.Move(k.FilePath, newFp); g = true; }
                            catch { }
                            if (g)
                            {
                                k.FilePath = newFp;
                            }
                        }
                        if (!k.FilePathThumb.Contains("[" + vidid + "]"))
                        {
                            string newFp = AppendToFilePath(k.FilePathThumb, " [" + vidid + "]");
                            try { File.Move(k.FilePathThumb, newFp); y = true; }
                            catch { }
                            if (y)
                            {
                                k.FilePathThumb = newFp;
                            }
                        }
                        if (g || y)
                        {
                            UpdateVideoData(db, k);
                        }
                    }
                }
            });
        }
        internal static async Task<bool> GetVideo(string dataFolder, Playlist k, string videoId, Vid v, Func<string, double, bool> progress, CookieContainer cookieJar)
        {
            DownloadResults results = new DownloadResults();
            results = await GetVideo(dataFolder, k.Name, videoId, progress, cookieJar, results);
            Global.Instance.Backoff.Reset();
            if (results.Success)
            {
                v.SetInfo(results.Info);
                v.FilePathThumb = results.FpMediaCoverArt;
                v.FilePath = results.FpVidSrc;
                v.Downloaded = true;
                v.TimeDownloadedDT = DateTime.Now;// = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                string rawDescription = results.Info.Description;
                v.Description = rawDescription.Replace("\r\n", "\n").Replace("\n", "<br/>\n");
                v.Url = GetVideoUrlFromId(videoId);
                v.UploadedDT = new DateTime(results.Info.UploadDate.Ticks, DateTimeKind.Local);
                return true;
            }
            return false;
        }
        internal static void SetTimesOnFiles(Vid v)
        {
            try
            {
                if (File.Exists(v.FilePath))
                {
                    File.SetCreationTime(v.FilePath, v.UploadedDT);
                    File.SetLastWriteTime(v.FilePath, v.UploadedDT);
                    File.SetLastAccessTime(v.FilePath, DateTime.Now);
                }
            }
            catch { }
            try
            {
                if (File.Exists(v.FilePathThumb))
                {
                    File.SetCreationTime(v.FilePathThumb, v.UploadedDT);
                    File.SetLastWriteTime(v.FilePathThumb, v.UploadedDT);
                    File.SetLastAccessTime(v.FilePathThumb, DateTime.Now);
                }
            }
            catch { }
            try
            {
                if (File.Exists(v.FilePathSrt))
                {
                    File.SetCreationTime(v.FilePathSrt, v.UploadedDT);
                    File.SetLastWriteTime(v.FilePathSrt, v.UploadedDT);
                    File.SetLastAccessTime(v.FilePathSrt, DateTime.Now);
                }
            }
            catch { }
            try
            {
                if (File.Exists(v.FilePathVtt))
                {
                    File.SetCreationTime(v.FilePathVtt, v.UploadedDT);
                    File.SetLastWriteTime(v.FilePathVtt, v.UploadedDT);
                    File.SetLastAccessTime(v.FilePathVtt, DateTime.Now);
                }
            }
            catch { }
        }
        internal static Vid GetExistingData(string url, SQLiteConnection db)
        {
            Vid v = new Vid();
            List<Vid> t = GetExistingRows(url, db);
            if (t.Count() > 0)
            {
                return t.First();
            }
            return v;
        }
        internal static List<Vid> GetVideos(SQLiteConnection db, string where = "")
        {
            List<Vid> Vids = new List<Vid>();
            try
            {
                string sql = string.Format("select * from video" + where);
                SQLiteCommand command = new SQLiteCommand(sql, db);
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Vid v = new Vid
                    {
                        Url = (string)reader["url"],
                        Title = (string)reader["title"],
                        Author = (string)reader["author"],
                        FilePath = (string)reader["filepath"],
                        FilePathThumb = (string)reader["filepaththumb"],
                        Keywords = (string)reader["keywords"],
                        Watched = (((long)reader["watched"]) == 0) ? false : true,
                        Downloaded = (((long)reader["downloaded"]) == 0) ? false : true,
                        TimeDownloaded = (long)reader["timedownloaded"],
                        Rating = (float)(double)reader["avg_rating"],
                        Uploaded = Convert.ToInt32((long)reader["uploaded"]),
                        Views = Convert.ToInt32((long)reader["view_count"]),
                        Length = Convert.ToInt32((long)reader["length_seconds"]),
                        Description = (string)reader["description"],
                        Id = Convert.ToInt32((long)reader["id"])
                    };
                    Vids.Add(v);
                }
            }
            catch (Exception) { }
            return Vids;
        }
        internal static List<PlaylistVidLookup> GetPlaylistVidLookups(SQLiteConnection db, string where = "")
        {
            List<PlaylistVidLookup> lists = new List<PlaylistVidLookup>();
            try
            {
                string sql = string.Format("select * from list_video_lookup" + where);
                SQLiteCommand command = new SQLiteCommand(sql, db);
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    lists.Add(new PlaylistVidLookup((int)(long)reader["id"], (int)(long)reader["video_id"], (int)(long)reader["list_id"]));
                }
            }
            catch { }
            return lists;
        }
        internal static List<Vid> GetExistingRows(string url, SQLiteConnection db)
        {
            List<Vid> Vids = new List<Vid>();
            string sql = string.Format("select * from video where url like '%{0}%'", url);
            SQLiteCommand command = new SQLiteCommand(sql, db);
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Vid v = new Vid
                {
                    Url = (string)reader["url"],
                    Title = (string)reader["title"],
                    Author = (string)reader["author"],
                    FilePath = (string)reader["filepath"],
                    FilePathThumb = (string)reader["filepaththumb"],
                    Keywords = (string)reader["keywords"],
                    Watched = (((long)reader["watched"]) == 0) ? false : true,
                    Downloaded = (((long)reader["downloaded"]) == 0) ? false : true,
                    TimeDownloaded = (long)reader["timedownloaded"],
                    Rating = (float)(double)reader["avg_rating"],
                    Uploaded = Convert.ToInt32((long)reader["uploaded"]),
                    Views = Convert.ToInt32((long)reader["view_count"]),
                    Length = Convert.ToInt32((long)reader["length_seconds"]),
                    Description = (string)reader["description"],
                    Id = Convert.ToInt32((long)reader["id"])
                };
                Vids.Add(v);
            }
            return Vids;
        }
        internal static List<Vid> VideoDownloaded(string id, SQLiteConnection db, bool checkforfile = true)
        {
            List<Vid> vids = GetExistingRows(id, db);
            if (checkforfile)
            {
                return vids.Where(k => k.Downloaded && File.Exists(k.FilePath)).ToList();
            }
            else
            {
                return vids.Where(k => k.Downloaded).ToList();
            }
        }
        internal static List<string> GetVideoRelatedFiles(string dataFolder, string url, ref List<string> files)
        {
            if (files == null || files.Count == 0)
            {
                files = Directory.GetFiles(dataFolder, "*.*", SearchOption.AllDirectories).ToList();
            }
            string vidid = "";
            if (url.StartsWith("http://") || url.StartsWith("https://"))
            {
                GetVideoIdFromUrl(url, ref vidid);
            }
            else
            {
                vidid = url;
            }
            return files.Where(k => k.Contains(" [" + vidid + "]")).ToList();
        }
        internal static Vid Sanertize(Vid v)
        {
            v.Url = v.Url.Replace("'", "''");
            v.Title = v.Title.Replace("'", "''");
            v.Author = v.Author.Replace("'", "''");
            v.FilePath = v.FilePath.Replace("'", "''");
            v.FilePathThumb = v.FilePathThumb.Replace("'", "''");
            v.Keywords = v.Keywords.Replace("'", "''");
            v.Description = v.Description.Replace("'", "''");
            return v;
        }
        internal static void InsertListVideoLookup(SQLiteConnection db, PlaylistVidLookup plvl)
        {
            string sql = string.Format(@"insert into list_video_lookup (video_id,list_id) values ({0},{1})", plvl.VideoId, plvl.ListId);
            SQLiteCommand command = new SQLiteCommand(sql, db);
            command.ExecuteNonQuery();
        }
        internal static void UpdateVideoData(SQLiteConnection db, Vid v)
        {
            Vid t = GetExistingData(v.Url, db);
            v = Sanertize(v);
            string sql = "";
            bool insert = false;
            if (t.Url != "")
            {
                sql = string.Format(
                    @"update video SET title = '{0}', filepath = '{1}', watched = {2}, downloaded = {3},
author = '{5}', timedownloaded = {6}, filepaththumb = '{7}', keywords = '{8}',
avg_rating = {9}, uploaded = {10}, view_count = {11}, length_seconds = {12}, description = '{13}'
where id = {14}",
                    v.Title, v._FilePath, (v.Watched) ? 1 : 0, (v.Downloaded) ? 1 : 0,
                    null, v.Author, v.TimeDownloaded, v._FilePathThumb, v.Keywords,
                    v.Rating, v.Uploaded, v.Views, v.Length, v.Description,
                    t.Id);
            }
            else
            {
                sql = string.Format(
                    @"insert into video (url,title,filepath,watched,downloaded,author,timedownloaded,
filepaththumb,keywords,avg_rating,uploaded,view_count,length_seconds,description
) values ('{0}','{1}','{2}',{3},{4},'{6}',{7},'{8}','{9}',{10},{11},{12},{13},'{14}')",
                    v.Url, v.Title, v._FilePath, (v.Watched) ? 1 : 0, (v.Downloaded) ? 1 : 0,
                    null, v.Author, v.TimeDownloaded, v._FilePathThumb, v.Keywords,
                    v.Rating, v.Uploaded, v.Views, v.Length, v.Description);
                insert = true;
            }
            SQLiteCommand command = new SQLiteCommand(sql, db);
            command.ExecuteNonQuery();
            if (insert)
            {
                v.Id = GetLastInsertedId(db);
            }
        }
        internal static int GetLastInsertedId(SQLiteConnection cnctn)
        {
            using (SQLiteCommand cmd = cnctn.CreateCommand())
            {
                cmd.CommandText = @"SELECT last_insert_rowid()";
                cmd.ExecuteNonQuery();
                int lastID = Convert.ToInt32(cmd.ExecuteScalar());
                return lastID;
            }
        }
        internal static Process PlayMedia(string inf1, Options options)
        {
            if (!File.Exists(inf1))
            {
                return null;
            }
            Process process = new Process();
            string swD3dfs = (options.D3dfs) ? " /d3dfs" : "";
            string swMon = " /monitor " + options.MonitorNumber.ToString();
            // Configure the process using the StartInfo properties.
            process.StartInfo.FileName = Constants.GetMPCPath();// @"C:\Program Files (x86)\K-Lite Codec Pack\MPC-HC64\mpc-hc64.exe";
            process.StartInfo.Arguments = string.Format("\"{0}\" /fullscreen /nofocus /play /close" + swMon + swD3dfs, inf1);
            process.StartInfo.WindowStyle = ProcessWindowStyle.Maximized;
            process.Start();
            return process;
        }
        internal static GetLibraryUrlResult GetPlaylists2(ref List<Playlist> pls)
        {
            try
            {
                pls = new List<Playlist>();
                if (!Global.BrowserGoTo(Constants.libraryUrl))
                {
                    return GetLibraryUrlResult.DocumentNotLoading;
                }
                if (!Global.BrowserWaitForIdle())
                {
                    return GetLibraryUrlResult.DocumentNotLoading;
                }
                HtmlDocument doc = new HtmlDocument();
                string source = Global.BrowserCurrentHTML;
                doc.LoadHtml(source);
                Global.BrowserGoTo("about:blank");
                foreach (HtmlNode playlistCard in doc.DocumentNode.SelectNodes("//*[@id=\"items\"]/ytd-grid-playlist-renderer").DefaultIfEmpty())
                {
                    Playlist pl = new Playlist() { Grab = false, Videos = new List<Vid>() };
                    pl.UrlThumb = playlistCard.SelectNodes(".//img").FirstOrDefault().Attributes["src"].Value.Replace("&amp;", "&");
                    HtmlNode plLink = playlistCard.SelectNodes(".//h3/a").FirstOrDefault();
                    pl.Name = plLink?.InnerText ?? "Unnamed Playlist";
                    pl.Url = plLink?.Attributes["href"].Value ?? null;
                    if (!string.IsNullOrWhiteSpace(pl.Url))
                    {
                        Match match = Regex.Match(pl.Url, "(?:\\&amp\\;|\\&|\\?)list=([^$]+)", RegexOptions.IgnoreCase);
                        if (match.Success)
                        {
                            pl.Url = match.Groups[1].Value;
                            pls.Add(pl);
                        }
                    }
                }
                if (pls.Count < 1)
                {
                    return GetLibraryUrlResult.CouldNotFindLink;
                }
                return GetLibraryUrlResult.FoundLink;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return GetLibraryUrlResult.CouldNotFindLink;
        }
        internal static List<string> TubeScroll(ref bool cancelled, Func<string, double, bool> progress, string progressString, int pageLimit = -1)
        {
            List<string> documents = new List<string>();
            try
            {
                HtmlDocument doc = null;
                progress("loading webpage", 0);
                Global.BrowserRunScript(@"
document.delItems = function(){ 
	var g = document.getElementsByTagName('ytd-video-renderer');
	for (var i = g.length - 1; i > -1; i--)
	{
		var p=g[i];
		p.parentNode.removeChild(p); 
	}
};");
                Global.BrowserRunScript(@"
document.noImg = function(){ 
	document.myImgs = document.getElementsByTagName('img');
	var f = document.lowerBnd;
	for (var i = document.myImgs.length - 1; i > -1; i--)
	{
		var p=document.myImgs[i];
		p.parentNode.removeChild(p); 
	}
	setTimeout(document.noImg, 100);
};");
                if (!Global.BrowserWaitForIdle())
                {
                    return documents;
                }
                while (true)
                {
                    if (cancelled)
                    {
                        return documents;
                    }
                    Global.BrowserRunScript("document.getElementsByTagName('ytd-browse')[0].scrollIntoView(false);");
                    if (!Global.BrowserWaitForIdle())
                    {
                        break;
                    }
                    doc = new HtmlDocument();
                    documents.Add(Global.BrowserCurrentHTML);
                    Thread.Yield();
                    progress(string.Format(progressString, documents.Count), 0);
                    Global.BrowserRunScript("document.delItems();");
                    if (!Global.BrowserWaitForIdle())
                    {
                        return documents;
                    }
                    if (documents.Count > 1)
                    {
                        if (documents[^1] == documents[^2])
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            {
                Debugger.Break();
                return documents;
            }
            finally
            {
            }
            return documents;
        }
        private static List<string> GetPages(string url, ref bool cancelled, Func<string, double, bool> progress, string progressString, int pageLimit = -1)
        {
            List<string> pages = null;
            WatiN.Core.Settings.AutoMoveMousePointerToTopLeft = false;
            try
            {
                WatiN.Core.Settings.AutoMoveMousePointerToTopLeft = false;
                Global.BrowserGoTo(url);
                Thread.Sleep(1000);
                pages = TubeScroll(ref cancelled, progress, progressString, pageLimit);
                if (cancelled)
                {
                    return new List<string>();
                }
            }
            catch (Exception)
            {
                Debugger.Break();
                return new List<string>();
            }
            return pages;
        }
        internal static List<VideoId> GetVideoIds(string url, ref bool cancelled, Func<string, double, bool> progress, string progressString, int pageLimit = -1)
        {
            List<string> pages = GetPages(url, ref cancelled, progress, progressString, -1);
            try
            {
                if (cancelled)
                {
                    return new List<VideoId>();
                }
                Thread.Sleep(500);
                try { Global.BrowserGoTo(Constants.blankPage); } catch { }
                List<VideoId> linkies = new List<VideoId>();
                foreach (string page in pages)
                {
                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(page);
                    foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//a[@href]"))
                    {
                        string link2 = HttpUtility.HtmlDecode(link.GetAttributeValue("href", string.Empty));
                        string vid = "";
                        if (GetVideoIdFromUrl(link2, ref vid))
                        {
                            try
                            {
                                VideoId vidid = new VideoId() { url = vid };
                                linkies.Add(vidid);
                                //grab exactly when it was posted, more recent = higher rez
                                string txt = link.InnerHtml;
                                if (!string.IsNullOrWhiteSpace(txt))
                                {
                                    Match mat = Regex.Match(txt, "(\\d+)\\s(\\w+)\\sago");
                                    Match mat2 = Regex.Match(txt, "just\\snow");
                                    if (mat.Success)
                                    {
                                        string number = mat.Groups[1].Value;
                                        string unit = mat.Groups[2].Value;
                                        vidid.UploadedTimeOffset = ParseFancyTime(number, unit);
                                    }
                                    else if (mat2.Success)
                                    {
                                        vidid.UploadedTimeOffset = TimeSpan.Zero;
                                    }
                                }
                            }
                            catch { }
                        }
                    }
                }
                //progress("getting videos", 100);
                return linkies.GroupBy(k => k.url).Select(g => g.First()).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return new List<VideoId>();
            }
        }
        internal static TimeSpan ParseFancyTime(string number, string unit)
        {
            try
            {
                int r = int.Parse(number);
                switch (unit.ToLower())
                {
                    case "second":
                        return new TimeSpan(0, 0, r);
                    case "seconds":
                        return new TimeSpan(0, 0, r);
                    case "minute":
                        return new TimeSpan(0, r, 0);
                    case "minutes":
                        return new TimeSpan(0, r, 0);
                    case "hour":
                        return new TimeSpan(r, 0, 0);
                    case "hours":
                        return new TimeSpan(r, 0, 0);
                }
            }
            catch { }
            return TimeSpan.MaxValue;
        }
    }
}