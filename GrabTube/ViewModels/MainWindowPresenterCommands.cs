﻿using System.Windows.Input;

namespace GrabTube.ViewModels
{
    public partial class MainWindowPresenter : Presenter
    {
        private bool CanSingleGrab => !Busy;

        private RelayCommand _SingleGrabCommand; public ICommand SingleGrabCommand { get { if (_SingleGrabCommand == null) { _SingleGrabCommand = new RelayCommand(param => SingleGrabStart(), param => CanSingleGrab); } return _SingleGrabCommand; } }

        private bool CanSetGrabFolder => true;

        private RelayCommand _SetGrabFolderCommand; public ICommand SetGrabFolderCommand { get { if (_SetGrabFolderCommand == null) { _SetGrabFolderCommand = new RelayCommand(param => SetGrabFolder(false), param => CanSetGrabFolder); } return _SetGrabFolderCommand; } }

        private bool CanLogin => !Busy;

        private RelayCommand _LoginCommand; public ICommand LoginCommand { get { if (_LoginCommand == null) { _LoginCommand = new RelayCommand(param => Login(), param => CanLogin); } return _LoginCommand; } }

        private bool CanAddList => true;

        private RelayCommand _AddListCommand; public ICommand AddListCommand { get { if (_AddListCommand == null) { _AddListCommand = new RelayCommand(param => UpdateList(), param => CanAddList); } return _AddListCommand; } }

        private bool CanDeleteList => null != win.lvLists.SelectedItem;

        private RelayCommand _DeleteListCommand; public ICommand DeleteListCommand { get { if (_DeleteListCommand == null) { _DeleteListCommand = new RelayCommand(param => DeleteList(), param => CanDeleteList); } return _DeleteListCommand; } }

        private bool CanStart => !Busy;

        private RelayCommand _StartCommand; public ICommand StartCommand { get { if (_StartCommand == null) { _StartCommand = new RelayCommand(param => Start(), param => CanStart); } return _StartCommand; } }

        private bool CanGatherLists => !Busy;

        private RelayCommand _GatherListsCommand; public ICommand GatherListsCommand { get { if (_GatherListsCommand == null) { _GatherListsCommand = new RelayCommand(param => UpdateLibraryAndLists(), param => CanGatherLists); } return _GatherListsCommand; } }

        private bool CanCancel => Busy;

        private RelayCommand _CancelCommand; public ICommand CancelCommand { get { if (_CancelCommand == null) { _CancelCommand = new RelayCommand(param => Cancel(), param => CanCancel); } return _CancelCommand; } }

        private bool CanExit => true;

        private RelayCommand _ExitCommand; public ICommand ExitCommand { get { if (_ExitCommand == null) { _ExitCommand = new RelayCommand(param => Exit(), param => CanExit); } return _ExitCommand; } }

        //private bool CanImportDatabase => !Busy;

        //private RelayCommand _ImportDatabaseCommand; public ICommand ImportDatabaseCommand { get { if (_ImportDatabaseCommand == null) { _ImportDatabaseCommand = new RelayCommand(param => ImportDatabase(), param => CanImportDatabase); } return _ImportDatabaseCommand; } }

        //bool CanUpdateData { get { return !Busy; } } RelayCommand _UpdateDataCommand; public ICommand UpdateDataCommand { get { if (_UpdateDataCommand == null) { _UpdateDataCommand = new RelayCommand(param => this.UpdateData(), param => this.CanUpdateData); } return _UpdateDataCommand; } }
        private bool CanExportDatabase => !Busy;

        private RelayCommand _ExportDatabaseCommand; public ICommand ExportDatabaseCommand { get { if (_ExportDatabaseCommand == null) { _ExportDatabaseCommand = new RelayCommand(param => ExportDatabase(), param => CanExportDatabase); } return _ExportDatabaseCommand; } }

        private bool CanAbout => true;

        private RelayCommand _AboutCommand; public ICommand AboutCommand { get { if (_AboutCommand == null) { _AboutCommand = new RelayCommand(param => About(), param => CanAbout); } return _AboutCommand; } }

        //private bool Canimpmine => true;

        //private RelayCommand _impmineCommand; public ICommand impmineCommand { get { if (_impmineCommand == null) { _impmineCommand = new RelayCommand(param => Impmine(), param => Canimpmine); } return _impmineCommand; } }

        private bool CanSearch => true;

        private RelayCommand _SearchCommand; public ICommand SearchCommand { get { if (_SearchCommand == null) { _SearchCommand = new RelayCommand(param => Search((string)param), param => CanSearch); } return _SearchCommand; } }

        private bool CanFeedWatch => !Busy;

        private RelayCommand _FeedWatchCommand; public ICommand FeedWatchCommand { get { if (_FeedWatchCommand == null) { _FeedWatchCommand = new RelayCommand(param => FeedWatchStart(), param => CanFeedWatch); } return _FeedWatchCommand; } }

        private bool CanOptions => true;

        private RelayCommand _OptionsCommand; public ICommand OptionsCommand { get { if (_OptionsCommand == null) { _OptionsCommand = new RelayCommand(param => Options(), param => CanOptions); } return _OptionsCommand; } }

        private bool CanFeedWatchServer => !Busy;

        private RelayCommand _FeedWatchServerCommand; public ICommand FeedWatchServerCommand { get { if (_FeedWatchServerCommand == null) { _FeedWatchServerCommand = new RelayCommand(param => FeedWatchServerStart(), param => CanFeedWatchServer); } return _FeedWatchServerCommand; } }

        private bool CanCheckForMissingOrDamagedVideos => !Busy;

        //private RelayCommand _CheckForMissingOrDamagedVideosCommand; public ICommand CheckForMissingOrDamagedVideosCommand { get { if (_CheckForMissingOrDamagedVideosCommand == null) { _CheckForMissingOrDamagedVideosCommand = new RelayCommand(param => CheckForMissingOrDamagedVideos(), param => CanCheckForMissingOrDamagedVideos); } return _CheckForMissingOrDamagedVideosCommand; } }
        //bool CanGetMissingMetadata { get { return !Busy; } } RelayCommand _GetMissingMetadataCommand; public ICommand GetMissingMetadataCommand { get { if (_GetMissingMetadataCommand == null) { _GetMissingMetadataCommand = new RelayCommand(param => this.GetMissingMetadata(), param => this.CanGetMissingMetadata); } return _GetMissingMetadataCommand; } }
    }
}
