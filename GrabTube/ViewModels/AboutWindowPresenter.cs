﻿using GrabTube.Views;

namespace GrabTube.ViewModels
{
    public class AboutWindowPresenter : Presenter
    {
        private readonly AboutWindow win = null;
        public AboutWindowPresenter(AboutWindow win)
            : base(win)
        {
            this.win = win;
        }

        public string Version => Constants.version;
        public string Product => Constants.appName;
        private delegate void voidMainWindowDelegate(MainWindow mw);
        public void Show(MainWindow win)
        {
            if (InvokeIfNecessary(new voidMainWindowDelegate(Show), win))
            {
                return;
            }

            this.win.Owner = win;
            this.win.ShowDialog();
        }
    }
}
