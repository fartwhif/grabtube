function next() {
    $("#nextBtn").prop("disabled", true);
    if (choices && choices.length > 1) {
        vid = choices[0];
        $('#streamPlayer source').attr('src', "stream.mp4?vid=" + vid + "&bitrate=" + bitrateChoice);
        $("#streamPlayer")[0].load();
        changing = true;
    }
}

$("#nextBtn").on("click", function () {
    next();
});

$("#streamPlayer")[0].addEventListener('ended', function () {
    next();
}, false);

var selBitrate = $('#selBitrate');
var bitrateChoice = "";
var vidAuthor = "";
var vidTitle = "";
var vidDesc = "";
var vid = "";
var choices = [];
var bitrates = [];
var changing = false;

selBitrate.on('change', function () {
    bitrateChoice = this.value;
    var playing = getPlaying();
    if (playing) {
        $('#streamPlayer source').attr('src', "stream.mp4?vid=" + vid + "&bitrate=" + bitrateChoice);
        $("#streamPlayer")[0].load();
        changing = true;
    }
});

function getPlaying(){
    return !($("#streamPlayer")[0].played.length < 1 && $("#streamPlayer")[0].networkState == 3);
}

function startLoop() {

    var r = function () {
        var playing = getPlaying();
        $.getJSON("update", { vid: vid, playing: playing })
          .done(function (data) {
              playing = getPlaying();
              if (changing)
                  changing = false;
              else {
                  if (data.Choices)
                      choices = data.Choices;
                  if (data.Bitrates) {
                      if (JSON.stringify(data.Bitrates) != JSON.stringify(bitrates)) {
                          bitrates = data.Bitrates;
                          var option = '';
                          for (var i = 0; i < bitrates.length; i++) {
                              option += '<option value="' + bitrates[i] + '">' + bitrates[i] + '</option>';
                          }
                          selBitrate.append(option);
                          bitrateChoice = data.Bitrates[0];
                      }
                  }
                  //console.log(JSON.stringify(data));
                  if (playing) {
                      if (data.Choices && data.Choices.length > 1)
                          $("#nextBtn").prop("disabled", false);
                      else
                          $("#nextBtn").prop("disabled", true);
                      if (data.CurrentlyPlayingAuthor) {
                          if (vidAuthor != data.CurrentlyPlayingAuthor) {
                              vidAuthor = data.CurrentlyPlayingAuthor;
                              $("#author").text(data.CurrentlyPlayingAuthor);
                          }
                      }
                      if (data.CurrentlyPlayingName) {
                          if (vidTitle != data.CurrentlyPlayingName) {
                              vidTitle = data.CurrentlyPlayingName;
                              $("#name").text(data.CurrentlyPlayingName);
                          }
                      }
                      if (data.CurrentlyPlayingDesc) {
                          if (vidDesc != data.CurrentlyPlayingDesc) {
                              vidDesc = data.CurrentlyPlayingDesc;
                              $("#desc").html(data.CurrentlyPlayingDesc.replace(new RegExp("href=\\\"/", "g"), "href=\"http://youtube.com/"));
                          }
                      }
                  }
                  else {
                      next();
                  }
              }
              setTimeout(r, 1000);
          })
          .fail(function (jqxhr, textStatus, error) {
              var err = textStatus + ", " + error;
              console.log("Request Failed: " + err);
              setTimeout(r, 1000);
          });
    }
    setTimeout(r, 1000);
}
startLoop();