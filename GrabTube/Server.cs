﻿using GrabTube.ViewModels;
using HttpMachine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace GrabTube
{

    internal class Server : IDisposable
    {
        public void Dispose()
        {
            server.ContinueListen = false;
            try
            {
                server.server.Stop();
            }
            catch (Exception) { }
            StrSvrMan.Shutdown = true;
        }

        private SocketServer server = null;
        private Playlist pl = null;
        private static Dictionary<string, string> GetCookies(Server.SocketServer.HttpRequest req)
        {
            if (req.Headers.ContainsKey("Cookie"))
            {
                return req.Headers["Cookie"].Split(';')
                    .Where(i => i.Contains("="))
                    .Select(i => i.Trim().Split('='))
                    .ToDictionary(i => i.First(), i => i.Last());
            }
            return new Dictionary<string, string>();
        }
        public void Listen(Options Settings, Playlist pl)
        {
            StrSvrMan.Shutdown = false;
            this.pl = pl;
            server = new SocketServer
            {
                //server.onLogMessage += (what) => { Console.WriteLine(what); };
                RequestHandler = (req) =>
                {
                    try
                    {
                        Dictionary<string, string> cookies = GetCookies(req);
                        if (!cookies.ContainsKey("session") || StrSvrMan.GetSession(cookies["session"]) == null)
                        {
                            StrSvrMan newSess = StrSvrMan.GetSession();
                            newSess.pl = this.pl;
                            newSess.HandleRequest(req, new Tuple<string, string>[] { new Tuple<string, string>("Set-Cookie", "session=" + newSess.Name + @";path=/") });
                        }
                        else
                        {
                            StrSvrMan.GetSession(cookies["session"]).HandleRequest(req);
                        }

                        if (req.Client.Connected)
                        {
                            req.NetworkStream.Dispose();
                        }
                    }
                    catch (Exception)
                    {
                        if (req.Client.Connected)
                        {
                            try { req.NetworkStream.Close(); }
                            catch { }
                        }
                        //Console.WriteLine(ex.Message);
                    }
                }
            };
            server.Listen(Settings);
        }

        public class SocketServer
        {
            public Action<HttpRequest> RequestHandler = null;
            public delegate void logDelegate(string what);
            public event logDelegate onLogMessage;
            private void log(string what)
            {
                if (onLogMessage != null)
                {
                    onLogMessage(what);
                }
            }
            public bool ContinueListen = true;
            public TcpListener server = null;
            public void Listen(Options settings)
            {

                try
                {
                    server = new TcpListener(IPAddress.Parse(settings.FeedWatchServerListenIP), settings.FeedWatchServerPort);
                    server.Start();

                }
                catch (Exception ex)
                {
                    log(ex.Message);
                    return;
                }
                new Thread((r) =>
                {
                    try
                    {
                        TcpListener srvr = (TcpListener)r;
                        while (ContinueListen)
                        {
                            DoBeginAcceptTcpClient(server);
                        }
                    }
                    catch (Exception ex)
                    {
                        log(ex.Message);
                    }
                }).Start(server);
            }
            // Thread signal.
            public ManualResetEvent tcpClientConnected = new ManualResetEvent(false);
            // Accept one client connection asynchronously.
            public void DoBeginAcceptTcpClient(TcpListener listener)
            {
                try
                {
                    // Set the event to nonsignaled state.
                    tcpClientConnected.Reset();
                    // Start to listen for connections from a client.
                    //Console.WriteLine("Waiting for a connection...");
                    // Accept the connection. 
                    // BeginAcceptSocket() creates the accepted socket.
                    listener.BeginAcceptTcpClient(
                        new AsyncCallback(DoAcceptTcpClientCallback),
                        listener);
                    // Wait until a connection is made and processed before 
                    // continuing.
                    tcpClientConnected.WaitOne();
                }
                catch (Exception ex)
                {
                    log(ex.Message);
                }
            }
            // Process the client connection.
            public void DoAcceptTcpClientCallback(IAsyncResult iar)
            {
                // Get the listener that handles the client request.
                //TcpListener listener = (TcpListener)ar.AsyncState;

                TcpListener l = (TcpListener)iar.AsyncState;
                TcpClient c;
                try
                {
                    c = l.EndAcceptTcpClient(iar);
                    new Thread((r) =>
                    {
                        try
                        {
                            parlay(c);
                        }
                        catch (Exception ex)
                        {
                            if (c.Connected)
                            {
                                try { c.Close(); }
                                catch { }
                            }

                            log(ex.Message);
                        }
                    }).Start(c);
                    tcpClientConnected.Set();
                }
                catch (SocketException)
                {
                    //Console.WriteLine("Error accepting TCP connection: {0}", ex.Message);

                    // unrecoverable
                    //_doneEvent.Set();
                    tcpClientConnected.Set();
                    return;
                }
                catch (ObjectDisposedException)
                {
                    // The listener was Stop()'d, disposing the underlying socket and
                    // triggering the completion of the callback. We're already exiting,
                    // so just return.
                    //Console.WriteLine("Listen canceled.");
                    tcpClientConnected.Set();
                    return;
                }
            }
            private void parlay(TcpClient clnt)
            {
                using (NetworkStream clientStream = clnt.GetStream())
                {
                    string strBuffer = string.Empty;
                    List<byte> buffer = new List<byte>();
                    byte[] q = new byte[8192];
                    MyHttpParser parserDele = new MyHttpParser((req) =>
                    {
                        if (RequestHandler != null)
                        {
                            req.NetworkStream = clientStream;
                            req.Client = clnt;
                            RequestHandler(req);
                        }
                    });
                    HttpMachine.HttpParser parser = new HttpMachine.HttpParser(parserDele);
                    while (clnt.Connected)
                    {
                        while (clnt.Connected && clientStream.DataAvailable)
                        {
                            int p = clientStream.Read(q, 0, 8192);
                            parser.Execute(new ArraySegment<byte>(q.Take(p).ToArray()));
                        }
                        Thread.Sleep(100);
                    }
                }
            }
            public class HttpRequest
            {
                public Dictionary<string, string> Headers = new Dictionary<string, string>();
                public string CurrentHeader = string.Empty;
                public string Method = string.Empty;
                public string Uri = string.Empty;
                public string QueryString = string.Empty;
                public string Path = string.Empty;
                public List<string> Fragments = new List<string>();
                public NetworkStream NetworkStream = null;
                public TcpClient Client = null;
            }
            public class MyHttpParser : IHttpParserHandler
            {
                public MyHttpParser(Action<HttpRequest> RequestHandler)
                    : base()
                {
                    this.RequestHandler = RequestHandler;
                }

                private readonly Action<HttpRequest> RequestHandler = null;
                private readonly HttpRequest request = new HttpRequest();
                public void OnBody(HttpParser parser, ArraySegment<byte> data)
                {
                }
                public void OnHeaderName(HttpParser parser, string name)
                {
                    if (!string.IsNullOrWhiteSpace(name))
                    {
                        request.Method = name;
                        request.Headers[name] = string.Empty;
                    }
                }
                public void OnHeadersEnd(HttpParser parser)
                {
                }
                public void OnHeaderValue(HttpParser parser, string value)
                {
                    if (!string.IsNullOrWhiteSpace(request.Method) && request.Headers.ContainsKey(request.Method))
                    {
                        request.Headers[request.Method] = value;
                    }
                }
                public void OnMessageBegin(HttpParser parser)
                {
                }
                public void OnMessageEnd(HttpParser parser)
                {
                    if (RequestHandler != null)
                    {
                        RequestHandler(request);
                    }
                }
                public void OnFragment(HttpParser parser, string fragment)
                {
                    request.Fragments.Add(fragment);
                }
                public void OnMethod(HttpParser parser, string method)
                {
                    request.Method = method;
                }
                public void OnPath(HttpParser parser, string path)
                {
                    request.Path = path;
                }
                public void OnQueryString(HttpParser parser, string queryString)
                {
                    request.QueryString = queryString;
                }
                public void OnRequestUri(HttpParser parser, string requestUri)
                {
                    request.Uri = requestUri;
                }
            }
        }
    }
}
