﻿namespace GrabTube
{
    internal class PlaylistVidLookup
    {
        public int Id { get; set; }
        public int VideoId { get; set; }
        public int ListId { get; set; }
        public PlaylistVidLookup() { }
        public PlaylistVidLookup(int Id, int VideoId, int ListId)
        {
            this.Id = Id;
            this.VideoId = VideoId;
            this.ListId = ListId;
        }
    }
}
