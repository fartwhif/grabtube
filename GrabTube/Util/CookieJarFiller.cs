﻿namespace GrabTube
{
    internal interface ICookieJarFiller
    {
        void FillCookieJar();
        void SetSniffURL(string url);
    }
}
