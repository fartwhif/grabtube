﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using YoutubeExplode.Videos.Streams;

namespace GrabTube
{
    internal class RebuildMedia
    {
        public static AudioType GetAudioType(IAudioStreamInfo info)
        {
            if (info.AudioCodec.ToLower().StartsWith("mp4a"))
            {
                return AudioType.mp4a;
            }
            AudioType type = AudioType.unknown;
            if (!Enum.TryParse<AudioType>(info.AudioCodec.ToLower(), out type))
            {
                Debugger.Break();
                throw new Exception($"unknown audio container name: {info.Container.Name}");
            }
            return type;
        }
        public static VideoType GetVideoType(IVideoStreamInfo info)
        {
            VideoType type = VideoType.unknown;
            if (!Enum.TryParse<VideoType>(info.Container.Name.ToLower(), out type))
            {
                Debugger.Break();
                throw new Exception($"unknown video container name: {info.Container.Name}");
            }
            return type;
        }
        public IVideoStreamInfo BestVid { get; set; }
        public IAudioStreamInfo BestAud { get; set; }
        public string FinalVideo { get; set; }
        public string FinalAudio { get; set; }
        public class Pass
        {
            public IVideoStreamInfo video { get; set; }
            public IAudioStreamInfo audio { get; set; }
            public bool KeepVideo { get; set; }
            public bool KeepAudio { get; set; }
            public bool ConvertVideo { get; set; }
            public bool ConvertAudio { get; set; }
        }
        public List<Pass> GetStrategy()
        {
            VideoType vidType = GetVideoType(BestVid);
            //AudioType audType = GetAudioType(BestAud);
            List<Pass> passes = new List<Pass>
                {
                    new Pass()
                    {
                        video = BestVid,
                        KeepVideo = true,
                        ConvertVideo = vidType != VideoType.mp4 && vidType != VideoType.h264 && vidType != VideoType.vp9 && vidType != VideoType.av1,
                        KeepAudio = false,
                        ConvertAudio = false
                    }
                };
            passes.Add(new Pass()
            {
                audio = BestAud,
                ConvertAudio = true,
                KeepAudio = true,
                KeepVideo = false,
            }); ;
            return passes;
        }
    }
}
