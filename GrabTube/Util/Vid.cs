﻿using GrabTube.ViewModels;
using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Media;
using YoutubeExplode.Videos;

namespace GrabTube
{
    public class Vid : INotifyPropertyChanged
    {
        public override int GetHashCode()
        {
            return Id;
        }
        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(Vid))
            {
                return false;
            }
            return Id == ((Vid)obj).Id;
        }
        public int Id { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
        public string _FilePath { get; set; }
        public string _FilePathThumb { get; set; }
        public string FilePathSrt => Path.Combine(Path.GetDirectoryName(FilePath), Path.GetFileNameWithoutExtension(FilePath) + ".srt");
        public string FilePathVtt => Path.Combine(Path.GetDirectoryName(FilePath), Path.GetFileNameWithoutExtension(FilePath) + ".vtt");
        public string FilePath
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_FilePath))
                {
                    return _FilePath;
                }
                return Path.Combine(Global.Instance.GrabFolder, _FilePath);
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value)) { _FilePath = value; return; }
                _FilePath = Path.Combine(new DirectoryInfo(Path.GetDirectoryName(value)).Name, Path.GetFileName(value));
            }
        }
        public string FilePathThumb
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_FilePathThumb))
                {
                    return _FilePathThumb;
                }
                return Path.Combine(Global.Instance.GrabFolder, _FilePathThumb);
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value)) { _FilePathThumb = value; return; }
                _FilePathThumb = Path.Combine(new DirectoryInfo(Path.GetDirectoryName(value)).Name, Path.GetFileName(value));
            }
        }
        public ImageSource ThumbImage
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(FilePathThumb) && File.Exists(FilePathThumb))
                {
                    return MainWindowPresenter.GetPic(FilePathThumb);
                }
                return null;
            }
        }
        public bool _Watched = false;
        public bool Watched { get => _Watched; set { _Watched = value; OnNotifyPropertyChanged(); } }
        public bool Downloaded { get; set; }
        public string Author { get; set; }
        public string Keywords { get; set; }
        public long TimeDownloaded { get; set; }
        public DateTime TimeDownloadedDT
        {
            get
            {
                DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                return dtDateTime.AddSeconds(TimeDownloaded).ToLocalTime();
            }
            set => TimeDownloaded = (int)(value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }
        public float Rating { get; set; }
        public int Uploaded { get; set; }
        public DateTime UploadedDT
        {
            get
            {
                DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                return dtDateTime.AddSeconds(Uploaded).ToLocalTime();
            }
            set => Uploaded = (int)(value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }
        public int Views { get; set; }
        public int Length { get; set; }
        public TimeSpan LengthTS => new TimeSpan(0, 0, Length);
        public string Description { get; set; }
        public Vid()
        {
            Id = 0;
            Url = string.Empty;
            Title = string.Empty;
            FilePath = string.Empty;
            FilePathThumb = string.Empty;
            Watched = false;
            Downloaded = false;
            Author = string.Empty;
            Keywords = string.Empty;
            TimeDownloaded = 0;
            Rating = 0;
            Uploaded = 0;
            Views = 0;
            Length = 0;
            Description = string.Empty;
        }
        public void SetInfo(Video info)
        {
            Author = (string.IsNullOrWhiteSpace(info.Author)) ? "" : info.Author;
            Keywords = (info.Keywords.Count > 0) ? (string.IsNullOrWhiteSpace(info.Keywords.Aggregate((a, b) => a + "," + b)) ? "" : info.Keywords.Aggregate((a, b) => a + "," + b)) : "";
            Rating = (string.IsNullOrWhiteSpace(info.Engagement.AverageRating.ToString())) ? 0 : float.Parse(info.Engagement.AverageRating.ToString());
            Views = (string.IsNullOrWhiteSpace(info.Engagement.ViewCount.ToString())) ? 0 : int.Parse(info.Engagement.ViewCount.ToString());
            Length = (string.IsNullOrWhiteSpace(info.Duration.TotalSeconds.ToString())) ? 0 : int.Parse(info.Duration.TotalSeconds.ToString());
            Title = (string.IsNullOrWhiteSpace(info.Title)) ? "" : info.Title;
        }
        public override string ToString()
        {
            return string.Format("\"{1}\" - \"{2}\" [{0}]", MainWindowPresenter.GetVideoIdFromUrl(Url), Author, Title);
        }
        public void SetInfoFromFilename(string filename)
        {
            string fn = Path.GetFileName(filename);
            Match match = Regex.Match(fn, "([^-]*){1}\\s-\\s(.+)\\s\\[(.+)\\]");
            if (match.Success)
            {
                Author = match.Groups[1].Value;
                Title = match.Groups[2].Value;
                Url = MainWindowPresenter.GetVideoUrlFromId(match.Groups[3].Value);
            }
        }
        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
        protected void OnNotifyPropertyChanged([CallerMemberName] string memberName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(memberName));
        }
        private static readonly string salt = "V3L2oBHsHszVnigZTvKjEc9hY742UXpZIC9";
        public bool Streamed { get; set; }
        private string _ExternalId = null;
        public string ExternalId
        {
            get
            {
                if (Url == null)
                {
                    return null;
                }
                if (_ExternalId != null)
                {
                    return _ExternalId;
                }
                MD5 md5 = MD5.Create();
                byte[] inputBytes = Encoding.ASCII.GetBytes(Url + salt);
                byte[] hash = md5.ComputeHash(inputBytes);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2"));
                }
                string t = sb.ToString();
                _ExternalId = t;
                return t;
            }
        }
        public string FilePathByBitrate(Bitrate bitrate)
        {
            switch (bitrate)
            {
                case Bitrate.High:
                    return FilePath;
                case Bitrate.Low:
                    return Path.GetDirectoryName(FilePath) + "\\" + Path.GetFileNameWithoutExtension(FilePath) + ".low.mp4";
                    //case Bitrate.SoundOnly:
                    //    return Path.GetDirectoryName(FilePath) + Path.GetFileNameWithoutExtension(FilePath) + ".aac";
            }
            return string.Empty;
        }
        public void EnsureQuality(Bitrate bitrate)
        {
            if (!File.Exists(FilePath))
            {
                return;
            }
            string fp = FilePathByBitrate(bitrate);
            string fileLock = fp + ".lock";
            DateTime st = DateTime.Now;
            if (bitrate == Bitrate.Low)
            {
                lock (lockLow)
                {
                    if (!File.Exists(fp))
                    {
                        MainWindowPresenter.VidTranscodeLow(FilePath, fp, (t, g) => { return true; });
                        File.SetCreationTime(fp, UploadedDT);
                        File.SetLastWriteTime(fp, UploadedDT);
                        File.SetLastAccessTime(fp, DateTime.Now);
                    }
                }
            }
        }
        public object lockLow = new object();
        //public object lockSoundOnly = new object();
    }
}
