﻿using System;

namespace GrabTube
{
    internal class VideoId
    {
        public string url { get; set; }
        public TimeSpan UploadedTimeOffset = TimeSpan.MaxValue;
    }
}
