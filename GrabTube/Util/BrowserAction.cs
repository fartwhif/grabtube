﻿using System;
using System.Threading;
using WatiN.Core;

namespace GrabTube
{
    public class BrowserAction
    {
        public Action<IE> Action { get; set; } = null;
        public ManualResetEvent CompletionToken { get; set; } = null;
        public Exception Exception { get; set; } = null;
    }
}
