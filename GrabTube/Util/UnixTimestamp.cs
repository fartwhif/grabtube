﻿using System;

namespace GrabTube
{
    internal static class UnixTimestamp
    {
        public static DateTime FromSeconds(int timestamp)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return dtDateTime.AddSeconds(timestamp).ToLocalTime();
        }
        public static int FromDateTime(DateTime dateTime)
        {
            return (int)(dateTime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }
    }
}
