﻿using YoutubeExplode.Videos;
using YoutubeExplode.Videos.Streams;

namespace GrabTube
{
    public class DownloadResults
    {
        public string FpMediaCoverArt { get; set; } = string.Empty;
        public bool Success { get; set; } = false;
        public string FpVidSrc { get; set; } = string.Empty;
        public Video Info { get; set; } = null;
        public IStreamInfo stream = null;
        public string FpSrt { get; set; }
        public string FpVtt { get; set; }
    }
}
