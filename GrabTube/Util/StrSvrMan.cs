﻿using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace GrabTube
{
    internal class StrSvrMan
    {
        public static bool Shutdown = false;
        private static string headerTuplesToString(Tuple<string, string>[] additionalHeadersToSet)
        {
            if (additionalHeadersToSet == null || additionalHeadersToSet.Length < 1)
            {
                return string.Empty;
            }

            string x = "";
            foreach (Tuple<string, string> header in additionalHeadersToSet)
            {
                x += header.Item1 + ": " + header.Item2 + "\r\n";
            }
            return x;
        }
        public Playlist pl = null;
        private readonly List<string> WatchedExternalIds = new List<string>();
        public void HandleRequest(Server.SocketServer.HttpRequest req, Tuple<string, string>[] additionalHeadersToSet = null)
        {
            switch (req.Path)
            {
                case "/":
                    serveFile(Constants.FileServer.Files.StreamingPage, req.NetworkStream, additionalHeadersToSet);
                    break;
                case "/update":
                    Availability avail = new Availability();
                    ClientUpdate clientUpd = ParseUpdate(req.QueryString);
                    Vid curVid = pl.Videos.Where(k => !k.Streamed && k.ExternalId == clientUpd.ExternalId).FirstOrDefault();
                    if (curVid != null)
                    {
                        curVid.Streamed = true;
                    }

                    curVid = pl.Videos.Where(k => k.ExternalId == clientUpd.ExternalId).FirstOrDefault();
                    if (curVid != null)
                    {
                        if (!WatchedExternalIds.Contains(clientUpd.ExternalId))
                        {
                            WatchedExternalIds.Add(clientUpd.ExternalId);
                        }

                        avail.CurrentlyPlayingAuthor = curVid.Author;
                        avail.CurrentlyPlayingName = curVid.Title;
                        avail.CurrentlyPlayingDesc = curVid.Description;
                    }
                    avail.Choices = pl.Videos.Where(k => !WatchedExternalIds.Contains(k.ExternalId))
                         .OrderByDescending(k => k.UploadedDT)
                         .Take(5)
                         .Select(a => a.ExternalId).ToList();
                    avail.Bitrates = Enum.GetNames(typeof(Bitrate)).ToList();
                    serveContent(JsonConvert.SerializeObject(avail), req.NetworkStream, additionalHeadersToSet);
                    break;
                case "/style.css":
                    serveFile(Constants.FileServer.Files.style, req.NetworkStream, additionalHeadersToSet);
                    break;
                case "/jquery.min.js":
                    serveFile(Constants.FileServer.Files.Jquery, req.NetworkStream, additionalHeadersToSet);
                    break;
                case "/app.js":
                    serveFile(Constants.FileServer.Files.AppJs, req.NetworkStream, additionalHeadersToSet);
                    break;
                case "/stream.mp4":
                    StreamRequest sr = ParseStreamRequest(req.QueryString);
                    Vid reqVid = pl.Videos.Where(k => k.ExternalId == sr.ExternalId).FirstOrDefault();
                    if (reqVid == null || !File.Exists(reqVid.FilePath))
                    {
                        byte[] byaResp4 = Encoding.UTF8.GetBytes(@"HTTP/1.1 500 Server Error
" + headerTuplesToString(additionalHeadersToSet) + @"
");
                        req.NetworkStream.Write(byaResp4, 0, byaResp4.Length);
                        break;
                    }

                    reqVid.EnsureQuality(sr.Bitrate);

                    using (FileStream fs = File.Open(reqVid.FilePathByBitrate(sr.Bitrate), FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        byte[] byaResp2 = Encoding.UTF8.GetBytes(@"HTTP/1.1 200 OK
Content-Type: video/mp4
Content-Length: " + fs.Length.ToString() + @"
" + headerTuplesToString(additionalHeadersToSet) + @"
");
                        req.NetworkStream.Write(byaResp2, 0, byaResp2.Length);
                        while (fs.Position < fs.Length && req.Client.Connected && !Shutdown)
                        {
                            byte[] buf = new byte[8192];
                            int a = fs.Read(buf, 0, 8192);
                            if (a > 0)
                            {
                                req.NetworkStream.Write(buf, 0, a);
                            }
                        }
                    }
                    break;
                default:
                    byte[] byaResp3 = Encoding.UTF8.GetBytes(@"HTTP/1.1 404 Not Found
" + headerTuplesToString(additionalHeadersToSet) + @"
");
                    req.NetworkStream.Write(byaResp3, 0, byaResp3.Length);
                    break;
            }
        }
        private Dictionary<string, string> ParseQueryString(string queryStr)
        {
            return queryStr.Split('&')
                .Where(i => i.Contains("="))
                .Select(i => i.Trim().Split('='))
                .ToDictionary(i => i.First(), i => i.Last());
        }
        private StreamRequest ParseStreamRequest(string queryStr)
        {
            Dictionary<string, string> x = ParseQueryString(queryStr);
            StreamRequest cu = new StreamRequest();
            if (x.ContainsKey("vid"))
            {
                cu.ExternalId = x["vid"];
            }

            if (x.ContainsKey("bitrate"))
            {
                Bitrate r = Bitrate.Low;
                if (Enum.TryParse<Bitrate>(x["bitrate"], out r))
                {
                    cu.Bitrate = r;
                }
            }
            return cu;
        }
        private ClientUpdate ParseUpdate(string queryStr)
        {
            Dictionary<string, string> x = ParseQueryString(queryStr);
            ClientUpdate cu = new ClientUpdate();
            if (x.ContainsKey("playing"))
            {
                cu.Playing = x["playing"] == "true";
            }

            if (x.ContainsKey("vid"))
            {
                cu.ExternalId = x["vid"];
            }

            return cu;
        }
        private class StreamRequest
        {
            public string ExternalId { get; set; }
            public Bitrate Bitrate = Bitrate.Low;
        }
        private class ClientUpdate
        {
            public bool Playing { get; set; }
            public string ExternalId { get; set; }
        }

        public class Availability
        {
            public List<string> Bitrates = new List<string>();
            public List<string> Choices = new List<string>();
            public string CurrentlyPlayingAuthor { get; set; }
            public string CurrentlyPlayingName { get; set; }
            public string CurrentlyPlayingDesc { get; set; }
        }

        private static readonly ConcurrentDictionary<string, StrSvrMan> Sessions = new ConcurrentDictionary<string, StrSvrMan>();
        public readonly string Name = string.Empty;
        public StrSvrMan(string Name)
        {
            this.Name = Name;
        }
        public static StrSvrMan GetSession(string sessionName)
        {
            if (Sessions.ContainsKey(sessionName))
            {
                return Sessions[sessionName];
            }

            return null;
        }
        private static readonly string sessionNameChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        public static StrSvrMan GetSession()
        {
            string x = string.Empty;
            Global.Random((rand) =>
            {
                for (int i = 0; i < 25; i++)
                {
                    x += sessionNameChars[rand.Next(0, sessionNameChars.Length)];
                }
            });
            if (Sessions.ContainsKey(x))
            {
                throw new Exception("randomness error");
            }

            StrSvrMan newSess = new StrSvrMan(x);
            Sessions.AddOrUpdate(x, newSess, (a, b) => b);
            return newSess;
        }
        private static void serveContent(string content, NetworkStream ns, Tuple<string, string>[] additionalHeadersToSet, string contentType = "application/json")
        {
            byte[] byaContent = Encoding.UTF8.GetBytes(content);
            byte[] byaResp = Encoding.UTF8.GetBytes(@"HTTP/1.1 200 OK
Content-Type: " + contentType + @"
Content-Length: " + byaContent.Length + @"
" + headerTuplesToString(additionalHeadersToSet) + @"
" + content);
            ns.Write(byaResp, 0, byaResp.Length);
        }
        private static void serveFile(Constants.FileServer.Files whichFile, NetworkStream ns, Tuple<string, string>[] additionalHeadersToSet)
        {
            byte[] byaResp = Encoding.UTF8.GetBytes(@"HTTP/1.1 200 OK
Content-Type: text/html
Content-Length: " + Constants.FileServer.GetFileBytes(whichFile).Length + @"
" + headerTuplesToString(additionalHeadersToSet) + @"
" + Constants.FileServer.GetFileString(whichFile));
            ns.Write(byaResp, 0, byaResp.Length);
        }
    }
}
