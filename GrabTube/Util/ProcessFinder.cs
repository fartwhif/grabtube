﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using WatiN.Core;

namespace GrabTube
{
    public class ProcessFinder
    {
        public static Win GetInternetExplorerWorkerInstancePID(IE ie, int tabId)
        {
            GetWindowThreadProcessId(ie.hWnd, out uint hostPid);
            Thread.Sleep(1000);
            List<Win> windows = EnumWindows();
            List<Win> e = windows.Where(k => !string.IsNullOrEmpty(k.WindowText)).Where(k => k.WindowText.ToLower().Contains("internet")).ToList();
            List<Win> f = e.Where(k => !string.IsNullOrEmpty(k.WindowText)).Where(k => k.WindowText.ToLower().Contains(tabId.ToString())).ToList();
            int y = f.RemoveAll(k => k.Pid == hostPid);
            return f.FirstOrDefault();
        }
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);
        private delegate bool EnumWindowsProc(IntPtr hWnd, IntPtr data);
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool EnumWindows(EnumWindowsProc lpEnumFunc, IntPtr data);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint processId);
        public static List<Win> EnumWindows()
        {
            List<Win> list = new List<Win>();
            EnumWindows((hWnd, lParam) =>
            {
                Win x = new Win { Handle = hWnd };
                StringBuilder sb = new StringBuilder(1024);
                GetClassName(hWnd, sb, sb.Capacity);
                x.ClassName = sb.ToString();
                sb = new StringBuilder(1024);
                GetWindowText(hWnd, sb, sb.Capacity);
                x.WindowText = sb.ToString();
                GetWindowThreadProcessId(hWnd, out uint r);
                x.Pid = r;
                list.Add(x);
                return true;

            }, IntPtr.Zero);
            return list;
        }
    }
}
