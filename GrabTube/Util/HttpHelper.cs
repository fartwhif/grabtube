﻿using System;
using System.Net;

namespace GrabTube
{
    public static class HttpHelper
    {
        public class CookieAwareWebClient : WebClient
        {
            //https://stackoverflow.com/questions/1777221/using-cookiecontainer-with-webclient-class
            public CookieContainer CookieJar = new CookieContainer();

            protected override WebRequest GetWebRequest(Uri address)
            {
                WebRequest request = base.GetWebRequest(address);
                if (request is HttpWebRequest webRequest)
                {
                    webRequest.CookieContainer = CookieJar;
                }
                return request;
            }
        }
    }
}