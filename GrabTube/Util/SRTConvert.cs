﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace GrabTube
{
    internal class SRTConvert
    {
        public static void ConvertTTMLToSRT(string InputTTMLFilePath, string OutputSRTFilePath)
        {
            SRTConvert conv = new SRTConvert(InputTTMLFilePath, OutputSRTFilePath);
            conv.ConvertToSrt();
        }
        public string OutputSRTFilePath { get; set; }
        public string InputTTMLFilePath { get; set; }
        public SRTConvert(string InputTTMLFilePath, string OutputSRTFilePath)
        {
            this.InputTTMLFilePath = InputTTMLFilePath;
            this.OutputSRTFilePath = OutputSRTFilePath;
        }

        private readonly TimeSpan cueEnd = TimeSpan.Zero;

        private class Entry
        {
            public TimeSpan Begin { get; set; }
            public TimeSpan End { get; set; }
            public string Text { get; set; }
        }

        private readonly List<Entry> AllEntries = new List<Entry>();


        private string CompileSRT()
        {
            StringBuilder outData = new StringBuilder();
            int entryId = 0;
            List<Entry> EntriesToCombine = new List<Entry>();
            TimeSpan Cue = TimeSpan.Zero;
            for (int i = 0; i < AllEntries.Count; i++)
            {
                Entry thisEntry = AllEntries[i];
                if (i == 0)
                {
                    Cue = thisEntry.End;
                }
                if (Cue == thisEntry.Begin || EntriesToCombine.Count > 1)
                {
                    Cue = thisEntry.End;
                    TimeSpan end = TimeSpan.FromTicks(Math.Min(thisEntry.Begin.Ticks, EntriesToCombine.Max(k => k.End).Ticks));
                    Entry combined = new Entry()
                    {
                        Begin = EntriesToCombine.Min(k => k.Begin),
                        End = end,
                        Text = EntriesToCombine.Select(k => k.Text).DefaultIfEmpty("").Aggregate((a, b) => a + " " + b)
                    };
                    WriteEntry(outData, combined, ++entryId);
                    EntriesToCombine.Clear();
                    EntriesToCombine.Add(thisEntry);
                }
                else
                {
                    EntriesToCombine.Add(thisEntry);
                }
            }
            if (EntriesToCombine.Any())
            {
                EntriesToCombine.ForEach(k => WriteEntry(outData, k, ++entryId));
            }
            return outData.ToString();
        }
        private static void WriteEntry(StringBuilder outData, Entry entry, int entryId)
        {
            if (outData.Length > 0)
            {
                outData.AppendLine();
            }
            outData.AppendLine(entryId.ToString());
            outData.Append(entry.Begin.ToString("h':'mm':'ss','fff"));
            outData.Append(" --> ");
            outData.Append(entry.End.ToString("h':'mm':'ss','fff"));
            outData.AppendLine();
            outData.AppendLine($"{entry.Text}");
        }
        public void ConvertToSrt()
        {
            try
            {
                XmlReader reader = XmlReader.Create(new StringReader(File.ReadAllText(InputTTMLFilePath)));
                XElement root = XElement.Load(reader);
                XmlNameTable nameTable = reader.NameTable;
                XmlNamespaceManager namespaceManager = new XmlNamespaceManager(nameTable);
                IEnumerable<XElement> paragraphs = root.XPathSelectElements("//body/p", namespaceManager);
                if (paragraphs.Any())
                {
                    foreach (XElement paragraph in paragraphs)
                    {
                        XAttribute time = paragraph.Attribute("t");
                        XAttribute duration = paragraph.Attribute("d");
                        IEnumerable<XElement> words = paragraph.XPathSelectElements("./s", namespaceManager);
                        string paraText = string.Empty;
                        foreach (XElement word in words)
                        {
                            XAttribute timeOffset = word.Attribute("t");
                            XAttribute ac = word.Attribute("ac");
                            string text = word.Value;
                            paraText += text;
                        }
                        if (time != null && duration != null && !string.IsNullOrWhiteSpace(paraText))
                        {
                            AllEntries.Add(new Entry()
                            {
                                Begin = TimeSpan.FromMilliseconds(int.Parse(time.Value)),
                                End = TimeSpan.FromMilliseconds(int.Parse(time.Value) + int.Parse(duration.Value)),
                                Text = paraText
                            });
                        }
                    }
                }
                else
                {
                    IEnumerable<XElement> paragraphs2 = root.XPathSelectElements("text");
                    foreach (XElement paragraph in paragraphs2)
                    {
                        XAttribute time = paragraph.Attribute("start");
                        XAttribute duration = paragraph.Attribute("dur");

                        string paraText = paragraph.Value;

                        if (time != null && duration != null && !string.IsNullOrWhiteSpace(paraText))
                        {
                            float _time = float.Parse(time.Value);
                            float _dur = float.Parse(duration.Value);

                            AllEntries.Add(new Entry()
                            {
                                Begin = TimeSpan.FromSeconds(_time),
                                End = TimeSpan.FromSeconds(_time + _dur),
                                //Text = System.Net.WebUtility.HtmlDecode(paraText)
                                Text = paraText.Replace("&#39;", "'")
                            });

                        }
                    }
                }
                File.WriteAllText(OutputSRTFilePath, CompileSRT());
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
