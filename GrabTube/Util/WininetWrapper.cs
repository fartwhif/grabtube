﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace GrabTube
{
    internal class WininetWrapper : ICookieJarFiller
    {
        /// <summary>
        /// Enables the retrieval of cookies that are marked as "HTTPOnly". 
        /// Do not use this flag if you expose a scriptable interface, 
        /// because this has security implications. It is imperative that 
        /// you use this flag only if you can guarantee that you will never 
        /// expose the cookie to third-party code by way of an 
        /// extensibility mechanism you provide. 
        /// Version:  Requires Internet Explorer 8.0 or later.
        /// </summary>
        private const int INTERNET_COOKIE_HTTPONLY = 0x00002000;

        public WininetWrapper()
        {
        }
        public WininetWrapper(string url)
        {
            SetSniffURL(url);
        }
        public void SetSniffURL(string url)
        {
            SniffUrl = url;
        }
        private string SniffUrl { get; set; } = null;
        public void FillCookieJar()
        {
            if (string.IsNullOrEmpty(SniffUrl))
            {
                BrowserShell.Instance.Cookies = null;
            }
            else
            {
                Thread fillerBackgroundThread = new Thread(Run);
                fillerBackgroundThread.SetApartmentState(ApartmentState.STA);
                fillerBackgroundThread.Start();
            }
        }
        private void Run()
        {
            _ = GetCookie(SniffUrl);
            if (!string.IsNullOrEmpty(SniffUrl))
            {
                Global.BrowserGoTo(SniffUrl);
                Thread.Sleep(100);
            }
            BrowserShell.Instance.Cookies = GetCookie(SniffUrl);
            if (!string.IsNullOrEmpty(SniffUrl))
            {
                Global.BrowserGoTo(Constants.blankPage);
            }
        }

        [DllImport("wininet.dll", SetLastError = true)]
        private static extern bool InternetGetCookieEx(string url, string cookieName, StringBuilder cookieData, ref int size, int flags, IntPtr pReserved);

        /// <summary>
        /// Returns cookie contents as a string
        /// requires elevated privileges the first call, 
        /// then after that even a new process without elevated will be able to sniff the HTTPONLY cookies
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private static string GetCookie(string url)
        {
            int size = 512;
            StringBuilder sb = new StringBuilder(size);
            if (!InternetGetCookieEx(url, null, sb, ref size, INTERNET_COOKIE_HTTPONLY, IntPtr.Zero))
            {
                if (size < 0)
                {
                    return null;
                }
                sb = new StringBuilder(size);
                if (!InternetGetCookieEx(url, null, sb, ref size, INTERNET_COOKIE_HTTPONLY, IntPtr.Zero))
                {
                    return null;
                }
            }
            return sb.ToString();
        }
    }
}
