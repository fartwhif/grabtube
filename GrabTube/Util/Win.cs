﻿using System;
using System.Diagnostics;

namespace GrabTube
{
    public class Win
    {
        public IntPtr Handle { get; set; }
        public string ClassName { get; set; }
        public string WindowText { get; set; }
        public uint Pid { get; set; }
        public string ExePath { get; set; }
        public Process Process { get; set; } = null;
    }
}
