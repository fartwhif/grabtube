﻿using System.Text.RegularExpressions;

namespace GrabTube
{
    public class ProbationReport
    {
        public string Stream_0_0 { get; set; }
        public string Stream_0_1 { get; set; }
        public bool HasVideo
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Stream_0_0) && Stream_0_0.Contains(": Video: "))
                {
                    return true;
                }

                if (!string.IsNullOrWhiteSpace(Stream_0_1) && Stream_0_1.Contains(": Video: "))
                {
                    return true;
                }

                return false;
            }
        }
        public bool HasAudio
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Stream_0_1) && Stream_0_1.Contains(": Audio: "))
                {
                    return true;
                }

                if (!string.IsNullOrWhiteSpace(Stream_0_0) && Stream_0_0.Contains(": Audio: "))
                {
                    return true;
                }

                return false;
            }
        }
        public bool HasBothVideoAndAudio => HasVideo && HasAudio;
        public static ProbationReport FromProbeOutput(string probeOutput)
        {
            ProbationReport pr = new ProbationReport();
            Match zero = Regex.Match(probeOutput, "Stream #0:0(.+)\\n");
            Match one = Regex.Match(probeOutput, "Stream #0:1(.+)\\n");
            if (zero.Success)
            {
                pr.Stream_0_0 = zero.Groups[1].Value.Trim();
            }
            if (one.Success)
            {
                pr.Stream_0_1 = one.Groups[1].Value.Trim();
            }
            return pr;
        }
        public override string ToString()
        {
            string s0 = (!string.IsNullOrWhiteSpace(Stream_0_0)) ? $", Stream #0:0{Stream_0_0}" : "";
            string s1 = (!string.IsNullOrWhiteSpace(Stream_0_1)) ? $", Stream #0:1{Stream_0_1}" : "";
            return $"Video: {HasVideo}, Audio: {HasAudio}{s0}{s1}";
        }
    }
}
