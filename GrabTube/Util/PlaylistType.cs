﻿namespace GrabTube
{
    public enum PlaylistType
    {
        Normal,
        SingleGrabs,
        Subscriptions,
        Library
    }
}
