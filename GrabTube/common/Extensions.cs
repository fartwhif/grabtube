﻿using System;
using System.Data.SQLite;
using System.Windows;
using System.Windows.Media;

namespace GrabTube
{
    /// <summary>
    /// https://stackoverflow.com/questions/315164/how-to-use-a-folderbrowserdialog-from-a-wpf-application
    /// </summary>
    internal static class Extensions
    {
        public static int ToBinaryInt(this bool bol)
        {
            return (bol) ? 1 : 0;
        }
        public static string SafeGetString(this SQLiteDataReader reader, string col)
        {
            if (!reader.IsDBNull(reader.GetOrdinal(col)))
            {
                return reader.GetString(reader.GetOrdinal(col));
            }

            return string.Empty;
        }
        public static int SafeGetInt(this SQLiteDataReader reader, string col)
        {
            if (!reader.IsDBNull(reader.GetOrdinal(col)))
            {
                return reader.GetInt32(reader.GetOrdinal(col));
            }

            return 0;
        }
        public static System.Windows.Forms.IWin32Window GetIWin32Window(this Visual visual)
        {
            System.Windows.Interop.HwndSource source = PresentationSource.FromVisual(visual) as System.Windows.Interop.HwndSource;
            System.Windows.Forms.IWin32Window win = new OldWindow(source.Handle);
            return win;
        }
        private class OldWindow : System.Windows.Forms.IWin32Window
        {
            private readonly IntPtr _handle;
            public OldWindow(IntPtr handle)
            {
                _handle = handle;
            }
            #region IWin32Window Members
            IntPtr System.Windows.Forms.IWin32Window.Handle => _handle;
            #endregion
        }
    }
}
