﻿namespace GrabTube
{
    public enum SW : int
    {
        HIDE = 0,
        SHOWNORMAL = 1,
        SHOWMINIMIZED = 2,
        SHOWMAXIMIZED = 3,
        SHOWNOACTIVATE = 4,
        SHOW = 5,
        MINIMIZE = 6,
        SHOWMINNOACTIVE = 7,
        SHOWNA = 8,
        RESTORE = 9,
        SHOWDEFAULT = 10
    }
    public enum THUMB_QUALITY
    {
        @default,
        hqdefault,
        mqdefault,
        sddefault
    }
    public enum AudioType
    {
        mp4a,
        none,
        aac,
        mp3,
        vorbis,
        opus,
        /// <summary>
        /// The audio type is unknown. This can occur if YoutubeExtractor is not up-to-date.
        /// </summary>
        unknown
    }
    /// <summary>
    /// The video type. Also known as video container.
    /// </summary>
    public enum VideoType
    {
        none,
        /// <summary>
        /// Video for mobile devices (3GP).
        /// </summary>
        mobile,
        flash,
        mp4,
        webm,
        av1,
        h264,
        //Mp4V,
        vp8,
        vp9,
        /// <summary>
        /// The video type is unknown. This can occur if YoutubeExtractor is not up-to-date.
        /// </summary>
        unknown
    }
    internal enum GetLibraryUrlResult
    {
        DocumentNotLoading,
        FoundLink,
        CouldNotFindLink,
        Unknown
    }
}
