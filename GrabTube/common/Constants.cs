﻿using System.IO;
using System.Reflection;
using System.Text;

namespace GrabTube
{
    internal class Constants
    {
        public const string obsoleteNoScriptAccess = "no script access in .net core, watin uses .net framework and lacks the marshalling";
        public const string validFileChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ _-[].01234567890";

        public const string protocol = "https";

        public const string appName = "GrabTube";
        public const string blankPage = "about:blank";

        public const string libraryUrl = protocol + "://youtube.com/feed/library";
        public const string subscriptionsUrl = protocol + "://youtube.com/feed/subscriptions";
        public const string urlAbout = protocol + "://youtube.com/about/";
        public const string urlAcct = protocol + "://youtube.com/account";
        public const string urlHistory = protocol + "://youtube.com/feed/history";
        public const string urlWatchLater = protocol + "://youtube.com/playlist?list=WL";
        public const string urlLikedList = protocol + "://youtube.com/playlist?list=LL";

        public const string urlTube = protocol + "://youtube.com";
        public const string tubeDomain = "youtube.com";
        public const string urlWatchUrl = protocol + "://youtube.com/watch?v=";
        public const string urlViewPlUrl = protocol + "://youtube.com/playlist?list=";
        public const string version = "0.31";
        public static string[] MPC = {
                                  "C:\\Program Files (x86)\\K-Lite Codec Pack\\MPC-HC64\\mpc-hc64_nvo.exe",
                                  "C:\\Program Files (x86)\\K-Lite Codec Pack\\MPC-HC\\mpc-hc_nvo.exe",
                                  "C:\\Program Files (x86)\\MPC-HC\\mpc-hc.exe",
                                  "C:\\Program Files\\K-Lite Codec Pack\\MPC-HC64\\mpc-hc64_nvo.exe",
                                  "C:\\Program Files\\K-Lite Codec Pack\\MPC-HC\\mpc-hc_nvo.exe",
                                  "C:\\Program Files\\MPC-HC\\mpc-hc.exe",
                                  @"C:\Program Files (x86)\K-Lite Codec Pack\MPC-HC64\mpc-hc64.exe"
                              };
        public static string GetMPCPath()
        {
            foreach (string k in MPC)
            {
                if (File.Exists(k))
                {
                    return k;
                }
            }
            return null;
        }
        public const bool browserInProc = false;
        public const bool showBrowser = false;

        public class FileServer
        {
            public enum Files
            {
                AppJs,
                Jquery,
                StreamingPage,
                style
            }
            public static byte[] GetFileBytes(Files which)
            {
                switch (which)
                {
                    case Files.style:
                        return StyleBytes;
                    case Files.AppJs:
                        return AppJsBytes;
                    case Files.Jquery:
                        return JQueryBytes;
                    case Files.StreamingPage:
                        return StreamingPageBytes;
                }
                return null;
            }
            public static string GetFileString(Files which)
            {
                switch (which)
                {
                    case Files.style:
                        return Style;
                    case Files.AppJs:
                        return AppJs;
                    case Files.Jquery:
                        return JQuery;
                    case Files.StreamingPage:
                        return StreamingPage;
                }
                return null;
            }
            private static string _Style = null;
            private static byte[] _StyleBytes = null;
            public static byte[] StyleBytes
            {
                get
                {
                    if (_StyleBytes != null)
                    {
                        return _StyleBytes;
                    }

                    _StyleBytes = Encoding.UTF8.GetBytes(Style);
                    return _StyleBytes;
                }
            }
            public static string Style
            {
                get
                {
                    if (_Style != null)
                    {
                        return _Style;
                    }

                    Assembly assembly = Assembly.GetExecutingAssembly();
                    using (Stream stream = assembly.GetManifestResourceStream(assembly.GetName().Name + ".server.style.css"))
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string file = reader.ReadToEnd();
                        _Style = file;

                        return _Style;
                    }
                }
            }
            private static string _AppJs = null;
            private static byte[] _AppJsBytes = null;
            public static byte[] AppJsBytes
            {
                get
                {
                    if (_AppJsBytes != null)
                    {
                        return _AppJsBytes;
                    }

                    _AppJsBytes = Encoding.UTF8.GetBytes(AppJs);
                    return _AppJsBytes;
                }
            }
            public static string AppJs
            {
                get
                {
                    if (_AppJs != null)
                    {
                        return _AppJs;
                    }

                    Assembly assembly = Assembly.GetExecutingAssembly();
                    using (Stream stream = assembly.GetManifestResourceStream(assembly.GetName().Name + ".server.app.js"))
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string file = reader.ReadToEnd();
                        _AppJs = file;

                        return _AppJs;
                    }
                }
            }
            private static string _JQuery = null;
            private static byte[] _JQueryBytes = null;
            public static byte[] JQueryBytes
            {
                get
                {
                    if (_JQueryBytes != null)
                    {
                        return _JQueryBytes;
                    }

                    _JQueryBytes = Encoding.UTF8.GetBytes(JQuery);
                    return _JQueryBytes;
                }
            }
            public static string JQuery
            {
                get
                {
                    if (_JQuery != null)
                    {
                        return _JQuery;
                    }

                    Assembly assembly = Assembly.GetExecutingAssembly();
                    using (Stream stream = assembly.GetManifestResourceStream(assembly.GetName().Name + ".server.jquery.min.js"))
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string file = reader.ReadToEnd();
                        _JQuery = file;

                        return _JQuery;
                    }
                }
            }
            private static string _StreamingPage = null;
            private static byte[] _StreamingPageBytes = null;
            public static byte[] StreamingPageBytes
            {
                get
                {
                    if (_StreamingPageBytes != null)
                    {
                        return _StreamingPageBytes;
                    }

                    _StreamingPageBytes = Encoding.UTF8.GetBytes(StreamingPage);
                    return _StreamingPageBytes;
                }
            }
            public static string StreamingPage
            {
                get
                {
                    if (_StreamingPage != null)
                    {
                        return _StreamingPage;
                    }

                    Assembly assembly = Assembly.GetExecutingAssembly();
                    using (Stream stream = assembly.GetManifestResourceStream(assembly.GetName().Name + ".server.stream.html"))
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string file = reader.ReadToEnd();
                        _StreamingPage = file;

                        return _StreamingPage;
                    }
                }
            }
        }
    }
}
