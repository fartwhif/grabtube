﻿using GrabTube.ViewModels;
using GrabTube.Views;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Threading;
using WatiN.Core;

namespace GrabTube
{
    public sealed class Global
    {
        private static readonly Lazy<Global> lazy =
            new Lazy<Global>(() => new Global());

        public static Global Instance => lazy.Value;
        private Global()
        {
            _rand = new Random();
            GrabFolder = string.Empty;
            BrowserQueue = new ConcurrentQueue<BrowserAction>();
            BrowserThread = new Thread(new ThreadStart(BrowserThreadLoop));
            BrowserThread.SetApartmentState(ApartmentState.STA);
            BrowserThread.Name = "BrowserThread";
            BrowserThreadAlive = true;
            BrowserThread.Start();
        }
        public string GrabFolder { get; set; }
        private IE _browser { get => browser1; set => browser1 = value; }
        public IE browser
        {
            get
            {
                browserReady.WaitOne();
                return _browser;
            }
        }
        public ExponentialBackoff Backoff { get; set; } = new ExponentialBackoff(5000, 900000);
        public Dispatcher MainWindowDispatcher { get; set; }
        public MainWindow MainWin { get; set; }
        public Thread BrowserThread { get; set; } = null;
        public bool BrowserThreadAlive = false;
        public ManualResetEvent BrowserWorkThreadEnd = new ManualResetEvent(false);
        public static void EndBrowserWork()
        {
            Instance.BrowserThreadAlive = false;
        }
        public static ManualResetEvent BrowserReady => browserReady;
        private static void BrowserThreadLoop()
        {
            Instance.BrowserWorkThreadEnd.Reset();
            while (Instance.BrowserThreadAlive)
            {
                Thread.Sleep(500);
                BrowserAction act = null;
                while (Instance.BrowserQueue.TryDequeue(out act) && Instance.BrowserThreadAlive)
                {
                    try
                    {
                        act.Action(Instance._browser);
                    }
                    catch (Exception ex) { act.Exception = ex; }
                    finally
                    {
                        act.CompletionToken.Set();
                    }
                }
            }
            Instance.BrowserWorkThreadEnd.Set();
        }
        public static void RunBrowserAction(Action<IE> act)
        {
            BrowserAction ba = new BrowserAction() { Action = act, CompletionToken = new ManualResetEvent(false) };
            Instance.BrowserQueue.Enqueue(ba);
            while (!ba.CompletionToken.WaitOne(500) && Instance.BrowserThreadAlive) { Thread.Yield(); }
            if (ba.Exception != null)
            {
                throw (ba.Exception);
            }
        }
        private readonly ConcurrentQueue<BrowserAction> BrowserQueue;
        public static bool BrowserGoTo(string url)
        {
            // To-Do: detect and handle IE "first time setup"
            RunBrowserAction(new Action<IE>((ie) =>
            {
                ie.GoTo(url);
            }));
            Thread.Sleep(1500);
            return true;
        }
        [Obsolete(Constants.obsoleteNoScriptAccess)]
        public static bool BrowserWaitForLoad()
        {
            bool x = false;
            RunBrowserAction(new Action<IE>((ie) =>
            {
                try
                {
                    string loaded = ie.Eval("(document.readyState === \"complete\");");
                    Stopwatch sw = new Stopwatch(); sw.Start();
                    while (loaded != "true")
                    {
                        if (sw.ElapsedMilliseconds > 15000) { x = false; return; }
                        Thread.Sleep(500);
                        loaded = ie.Eval("(document.readyState === \"complete\");");
                    }
                    x = true;
                }
                catch (Exception) { Debugger.Break(); }
            }));
            return x;
        }
        public static bool BrowserWaitForIdle()
        {
            bool x = false;
            try
            {
                //determine moment IE process qualifies as idle
                TimeSpan idleBelow = TimeSpan.FromMilliseconds(30); // for an i7 4790k seems OK
                TimeSpan minDuration = TimeSpan.FromSeconds(4);
                Stopwatch elapsed = new Stopwatch();
                TimeSpan prevTime = IEProcessorTime;
                List<long> samples = new List<long>();
                RunBrowserAction(new Action<IE>((ie) =>
                {
                    while (true)
                    {
                        TimeSpan p = IEProcessorTime;
                        TimeSpan used = p - prevTime;
                        prevTime = p;
                        samples.Add(used.Ticks);
                        if (samples.Count > 10)
                        {
                            samples.RemoveAt(0);
                        }
                        if (samples.Count == 10 && (long)samples.Average() < idleBelow.Ticks)
                        {
                            if (elapsed.Elapsed >= minDuration)
                            {
                                x = true;
                                break;
                            }
                            else if (!elapsed.IsRunning)
                            {
                                elapsed.Restart();
                            }
                        }
                        else
                        {
                            elapsed.Stop();
                        }
                        Thread.Sleep(250);
                    }
                }));
            }
            catch { }
            return x;
        }
        public static string BrowserCurrentHTML => BrowserGetCurrentHTML();
        public static void BrowserStopThread()
        {
            Instance.BrowserThreadAlive = false;
        }
        public static string BrowserGetCurrentHTML()
        {
            string x = null;
            RunBrowserAction(new Action<IE>((ie) =>
            {
                x = Instance.browser.Html;
            }));
            return x;
        }
        public static string BrowserEval(string statement)
        {
            string x = null;
            RunBrowserAction(new Action<IE>((ie) =>
            {
                x = ie.Eval(statement);
            }));
            return x;
        }
        public static uint BrowserGetPID()
        {
            int pid = -1;
            RunBrowserAction((ie) =>
            {
                if (Instance._browser != null)
                {
                    try
                    {
                        pid = Instance._browser.ProcessID;
                    }
                    catch (Exception)
                    {
                    }
                }
            });
            return (uint)pid;
        }
        public static void BrowserRunScript(string script)
        {
            RunBrowserAction((ie) => { ie.RunScript(script); });
        }
        public static void StartBrowser()
        {
            browserReady.Reset();
            int tabId = 0;
            RunBrowserAction(new Action<IE>((ie) =>
            {
                IE browser = ie;
                if (browser != null)
                {
                    try
                    {
                        browser.ForceClose();
                        browser.Dispose();
                    }
                    catch { }
                }
                string hint = @"C:\Program Files\Internet Explorer\iexplore.exe";
                if (File.Exists(hint))
                {
                    string pathvar = Environment.GetEnvironmentVariable("PATH");
                    Environment.SetEnvironmentVariable("PATH", @$"{pathvar};{Path.GetDirectoryName(hint)}");
                }
                browser = new IE(Constants.browserInProc)
                {
                    Visible = Instance.ShowBrowser
                };
                Instance._browser = browser;
            }));
            Random((rng) => tabId = rng.Next(0, int.MaxValue));
            BrowserGoTo("about:blank");
            RunBrowserAction(new Action<IE>((ie) =>
            {
                ie.RunScript(@$"
document.tabId1 = function(){{ 
	document.title = ""{tabId}"";
}};
document.tabId2 = function(){{
    setTimeout(document.tabId1, 100);
}};");
                ie.RunScript("document.tabId2();");
                Instance._IETab = ProcessFinder.GetInternetExplorerWorkerInstancePID(ie, tabId);
                if (Instance._IETab != null)
                {
                    Instance._IETab.Process = Process.GetProcessById((int)Instance._IETab.Pid);
                }
            }));
            if (Instance._IETab == null)
            {
                throw new Exception("Failed to find PID of Internet Explorer worker process.");
            }
            browserReady.Set();
        }
        private readonly Random _rand = null;
        public static void Random(Action<Random> randomAction)
        {
            lock (Instance._rand)
            {
                randomAction(Instance._rand);
            }
        }
        private Win _IETab = null;
        public static Win IETab => Instance._IETab;
        public static TimeSpan IEProcessorTime
        {
            get
            {
                Process p = Instance._IETab.Process;
                p.Refresh();
                return p.TotalProcessorTime;
            }
        }
        public static readonly ManualResetEvent browserReady = new ManualResetEvent(false);
        public static void BrowserSetShowBrowser(bool show)
        {
            if (show != Instance.ShowBrowser)
            {
                Instance.ShowBrowser = show;
                RunBrowserAction(new Action<IE>((ie) =>
                {
                    int param = show ? (int)SW.SHOW : (int)SW.HIDE;
                    ShowWindowAsync((int)Instance._browser.hWnd, param);
                }));
            }
        }
        public bool ShowBrowser = false;
        private IE browser1;

        [DllImport("user32.dll")]
        public static extern bool ShowWindowAsync(int hWnd, int nCmdShow);
    }
}