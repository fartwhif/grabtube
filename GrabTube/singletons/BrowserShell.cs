﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using YoutubeExplode;
using static GrabTube.HttpHelper;

namespace GrabTube
{
    public sealed class BrowserShell
    {
        private static readonly Lazy<BrowserShell> lazy =
            new Lazy<BrowserShell>(() => new BrowserShell());
        public static BrowserShell Instance => lazy.Value;

        private BrowserShell()
        {
        }
        public void FireDownloadException(Exception ex)
        {
            Instance.OnDownloadException?.Invoke(ex);
        }
        public void FireDownloadAttemptInner(HttpWebRequest req)
        {
            Instance.OnDownloadAttemptInner?.Invoke(req);
        }
        public void FireDownloadAttemptOuter(string url)
        {
            Instance.OnDownloadAttemptOuter?.Invoke(url);
        }
        public delegate void DownloadAttemptInnerDelegate(HttpWebRequest req);
        public delegate void DownloadExceptionDelegate(Exception ex);
        public delegate void DownloadAttemptOuterDelegate(string url);
        public event DownloadExceptionDelegate OnDownloadException;
        public event DownloadAttemptInnerDelegate OnDownloadAttemptInner;
        public event DownloadAttemptOuterDelegate OnDownloadAttemptOuter;
        private YoutubeClient __explodeClient;
        private YoutubeClient _explodeClient
        {
            get
            {
                HttpClientHandler handler = new HttpClientHandler();
                HttpClient client = new HttpClient(handler);
                client.DefaultRequestHeaders.Add("Cookie", Instance.Cookies);
                if (__explodeClient == null)
                {
                    __explodeClient = new YoutubeClient(client);
                    return __explodeClient;
                }
                else
                {
                    return new YoutubeClient(client);
                }
            }
        }
        internal static YoutubeClient ExplodeClient => Instance._explodeClient;
        public string Cookies { get; set; }
        public static HttpWebRequest NewWebRequest(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.CookieContainer = null;
            request.Headers.Add(HttpRequestHeader.Cookie, Instance.Cookies);
            request.Headers.Add(HttpRequestHeader.AcceptLanguage, "en-US");
            request.Headers.Add("DNT", "1");
            request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko";
            request.Accept = "*/*";
            return request;
        }
        public static CookieAwareWebClient NewWebClient()
        {
            CookieAwareWebClient client = new CookieAwareWebClient
            {
                CookieJar = null
            };
            client.Headers.Add(HttpRequestHeader.Cookie, Instance.Cookies);
            client.Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko");
            client.Headers.Add(HttpRequestHeader.Accept, "*/*");
            client.Headers.Add(HttpRequestHeader.AcceptLanguage, "en-US");
            client.Headers.Add("DNT", "1");
            client.Encoding = Encoding.UTF8;
            return client;
        }
    }
}
