[![pipeline status](https://gitlab.com/fartwhif/grabtube/badges/master/pipeline.svg)](https://gitlab.com/fartwhif/grabtube/-/commits/master) [![coverage report](https://gitlab.com/fartwhif/grabtube/badges/master/coverage.svg)](https://gitlab.com/fartwhif/grabtube/-/commits/master)

# GrabTube

A list oriented dirty video archiving application. These days content is removed regularly from the public square maliciously for the purposes of preventing unauthorized ideas and information from propagating, therefore archiving is important.  When you think you might want to share something archive it as a precaution against malicious political censorship.  GrabTube also provides a way to steadily watch content feeds as new content notifications are discovered without babysitting some website.  Create an unattended video streaming website for a content feed.

Currently "dirtily supports" only YouTube, because reasons...

Depends on [YoutubeExplode](https://github.com/Tyrrrz/YoutubeExplode) in conjunction with Internet Explorer to interact with YouTube.

## features:
- grab playlist libraries
- grab public or private playlists
- handles all playlist types, including liked, watch later, favorites, history, subscriptions, and custom playlists.
- grab single videos
- retains description and metadata
- search & sort grabbed videos
- ~~feedwatch - continuously grab & play new subscriptions feed uploads as they arrive~~
- ~~feedwatch web based streaming server~~
- support for 4K and 60FPS video
